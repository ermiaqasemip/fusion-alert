function checkHoverTouchState() {
	var isTouch = false,
		isTouchTimer;

	function addtouchclass( e ) {
		clearTimeout( isTouchTimer );
		isTouch = true;

		jQuery( 'body' ).addClass( 'fusion-touch' );
		jQuery( 'body' ).removeClass( 'fusion-no-touch' );

		isTouchTimer = setTimeout( function() {
			isTouch = false;
		}, 500 );
	}

	function removetouchclass( e ) {
		if ( ! isTouch ) {
			isTouch = false;
			jQuery( 'body' ).addClass( 'fusion-no-touch' );
			jQuery( 'body' ).removeClass( 'fusion-touch' );
		}
	}

	document.addEventListener( 'touchstart', addtouchclass );
	document.addEventListener( 'mouseover', removetouchclass );
}

checkHoverTouchState();

jQuery( document ).ready( function() {
	jQuery( 'input, textarea' ).placeholder();
} );

// Need to add same thing to FB if we want to use it.
jQuery( window ).load( function() {
	jQuery( '.fusion-youtube-flash-fix' ).remove();
} );
