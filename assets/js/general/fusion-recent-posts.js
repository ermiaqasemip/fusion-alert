jQuery( window ).load( function() {

	// Setup infinite scroll for each recent posts element.
	jQuery( '.fusion-recent-posts-infinite .fusion-columns' ).each( function() {

		// Set the correct container for recent posts infinite scroll.
		var recentPostsInfiniteContainer = jQuery( this ),
			recentPostsWrapper = recentPostsInfiniteContainer.parent(),
			recentPostsWrapperClasses = '.' + recentPostsWrapper.attr( 'class' ).replace( /\ /g, '.' ) + ' ',
			originalPosts        = jQuery( this ).find( '.fusion-column' ),
			currentPage,
			loadMoreButton;

		jQuery( recentPostsInfiniteContainer ).infinitescroll( {

			navSelector: recentPostsWrapperClasses + '.fusion-infinite-scroll-trigger',

			// Selector for the paged navigation (it will be hidden).
			nextSelector: recentPostsWrapperClasses + 'a.pagination-next',

			// Selector for the NEXT link (to page 2).
			itemSelector: recentPostsWrapperClasses + 'div.pagination .current, ' + recentPostsWrapperClasses + 'article.post',

			// Selector for all items you'll retrieve.
			loading: {
				finishedMsg: fusionRecentPostsVars.infinite_finished_msg,
				msg: jQuery( '<div class="fusion-loading-container fusion-clearfix"><div class="fusion-loading-spinner"><div class="fusion-spinner-1"></div><div class="fusion-spinner-2"></div><div class="fusion-spinner-3"></div></div><div class="fusion-loading-msg">' + fusionRecentPostsVars.infinite_loading_text + '</div>' )
			},

			maxPage: ( recentPostsWrapper.data( 'pages' ) ) ? recentPostsWrapper.data( 'pages' ) : undefined,

			errorCallback: function() {
			}
		}, function( posts ) {

			jQuery( posts ).hide();

			// Add and fade in new posts when all images are loaded
			imagesLoaded( posts, function() {
				jQuery( posts ).fadeIn();
			} );

			// Initialize flexslider for post slideshows.
			jQuery( recentPostsInfiniteContainer ).find( '.flexslider' ).flexslider( {
				slideshow: Boolean( Number( fusionRecentPostsVars.slideshow_autoplay ) ),
				slideshowSpeed: fusionRecentPostsVars.slideshow_speed,
				video: true,
				smoothHeight: false,
				pauseOnHover: false,
				useCSS: false,
				prevText: '&#xf104;',
				nextText: '&#xf105;',
				start: function( slider ) {

					// Remove Loading
					slider.removeClass( 'fusion-flexslider-loading' );

					if ( 'undefined' !== typeof( slider.slides ) && 0 !== slider.slides.eq( slider.currentSlide ).find( 'iframe' ).length ) {
						if ( Number( fusionRecentPostsVars.pagination_video_slide ) ) {
							jQuery( slider ).find( '.flex-control-nav' ).css( 'bottom', '-20px' );
						} else {
							jQuery( slider ).find( '.flex-control-nav' ).hide();
						}
						if ( Number( fusionRecentPostsVars.status_yt ) && true === window.yt_vid_exists ) {
							window.YTReady( function() {
								new YT.Player( slider.slides.eq( slider.currentSlide ).find( 'iframe' ).attr( 'id' ), {
									events: {
										'onStateChange': onPlayerStateChange( slider.slides.eq( slider.currentSlide ).find( 'iframe' ).attr( 'id' ), slider )
									}
								} );
							} );
						}
					} else {
						if ( Number( fusionRecentPostsVars.pagination_video_slide ) ) {
							jQuery( slider ).find( '.flex-control-nav' ).css( 'bottom', '0px' );
						} else {
							jQuery( slider ).find( '.flex-control-nav' ).show();
						}
					}

					// Reinitialize waypoints
					jQuery.waypoints( 'viewportHeight' );
					jQuery.waypoints( 'refresh' );
				},
				before: function( slider ) {
					if ( 0 !== slider.slides.eq( slider.currentSlide ).find( 'iframe' ).length ) {
						if ( Number( fusionRecentPostsVars.status_vimeo ) && -1 !== slider.slides.eq( slider.currentSlide ).find( 'iframe' )[0].src.indexOf( 'vimeo' ) ) {
							new Vimeo.Player( slider.slides.eq( slider.currentSlide ).find( 'iframe' )[0] ).pause();
						}

						if ( Number( fusionRecentPostsVars.status_yt ) && true === window.yt_vid_exists ) {
							window.YTReady( function() {
								new YT.Player( slider.slides.eq( slider.currentSlide ).find( 'iframe' ).attr( 'id' ), {
									events: {
										'onStateChange': onPlayerStateChange( slider.slides.eq( slider.currentSlide ).find( 'iframe' ).attr( 'id' ), slider )
									}
								} );
							} );
						}
					}
				},
				after: function( slider ) {
					if ( 0 !== slider.slides.eq( slider.currentSlide ).find( 'iframe' ).length ) {
						if ( Number( fusionRecentPostsVars.pagination_video_slide ) ) {
							jQuery( slider ).find( '.flex-control-nav' ).css( 'bottom', '-20px' );
						} else {
							jQuery( slider ).find( '.flex-control-nav' ).hide();
						}

						if ( Number( fusionRecentPostsVars.status_yt ) && true === window.yt_vid_exists ) {
							window.YTReady( function() {
								new YT.Player( slider.slides.eq( slider.currentSlide ).find( 'iframe' ).attr( 'id' ), {
									events: {
										'onStateChange': onPlayerStateChange( slider.slides.eq( slider.currentSlide ).find( 'iframe' ).attr( 'id' ), slider )
									}
								} );
							} );
						}
					} else {
						if ( Number( fusionRecentPostsVars.pagination_video_slide ) ) {
							jQuery( slider ).find( '.flex-control-nav' ).css( 'bottom', '0px' );
						} else {
							jQuery( slider ).find( '.flex-control-nav' ).show();
						}
					}
					jQuery( '[data-spy="scroll"]' ).each( function() {
						var $spy = jQuery( this ).scrollspy( 'refresh' ); // jshint ignore:line
					} );
				}
			} );

			// Trigger fitvids
			jQuery( posts ).each( function() {
				jQuery( this ).find( '.full-video, .video-shortcode, .wooslider .slide-content' ).fitVids();
			} );

			// Hide the load more button, if the currently loaded page is already the last page.
			currentPage = recentPostsWrapper.find( '.current' ).html();
			recentPostsWrapper.find( '.current' ).remove();

			if ( recentPostsWrapper.data( 'pages' ) == currentPage ) { // jshint ignore:line
				recentPostsWrapper.find( '.fusion-loading-container' ).hide();
				recentPostsWrapper.find( '.fusion-load-more-button' ).hide();
			}

			// Activate lightbox for the newly added posts
			if ( 'individual' === fusionRecentPostsVars.lightbox_behavior || ! originalPosts.find( '.fusion-post-slideshow' ).length ) {
				window.avadaLightBox.activate_lightbox( jQuery( posts ) );

				originalPosts = recentPostsInfiniteContainer.find( '.post' );
			}

			// Refresh the lightbox, needed in any case
			window.avadaLightBox.refresh_lightbox();
			jQuery( window ).trigger( 'resize', [ false ] );

			// Trigger resize so that any parallax sections below get recalculated.
			setTimeout( function() {
				jQuery( window ).trigger( 'resize', [ false ] );
			}, 500 );

			// Reinitialize waypoints.
			if ( jQuery.isFunction( jQuery.fn.initWaypoint ) ) {
				jQuery( window ).initWaypoint();
			}

			// Reinitialize nice scroll, if used.
			if ( 'undefined' !== typeof niceScrollReInit ) {
				niceScrollReInit();
			}
		} );

		// Setup infinite scroll manual loading
		if ( jQuery( recentPostsWrapper ).hasClass( 'fusion-recent-posts-load-more' ) ) {
			jQuery( recentPostsInfiniteContainer ).infinitescroll( 'unbind' );

			// Load more posts button click.
			loadMoreButton = jQuery( recentPostsWrapper ).find( '.fusion-load-more-button' );

			loadMoreButton.on( 'click', function( e ) {
				e.preventDefault();

				// Use the retrieve method to get the next set of posts.
				jQuery( recentPostsInfiniteContainer ).infinitescroll( 'retrieve' );

			} );
		}

		// Hide the load more button, if there is only one page.
		if ( 1 === parseInt( recentPostsWrapper.data( 'pages' ), 10 ) ) {
			recentPostsWrapper.find( '.fusion-loading-container' ).hide();
			recentPostsWrapper.find( '.fusion-load-more-button' ).hide();
		}
	} );
} );
