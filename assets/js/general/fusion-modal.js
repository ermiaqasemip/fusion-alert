/* global Vimeo, generateCarousel, fusionResizeCrossfadeImagesContainer, fusionResizeCrossfadeImages, calcSelectArrowDimensions */
function getScrollBarWidth() {
	var $outer = jQuery( '<div>' ).css( { visibility: 'hidden', width: 100, overflow: 'scroll' } ).appendTo( 'body' ),
		$widthWithScroll = jQuery( '<div>' ).css( { width: '100%' } ).appendTo( $outer ).outerWidth();
	$outer.remove();
	return 100 - $widthWithScroll;
}
jQuery( window ).load( function() { // Start window_load_1

	var scrollbarWidth = parseFloat( getScrollBarWidth() );

	// Initialize Bootstrap Modals
	jQuery( '.fusion-modal' ).each( function() {

		// Changed from #wrapper to body.
		jQuery( 'body' ).append( jQuery( this ) );
	} );
	jQuery( '.fusion-modal' ).bind( 'hidden.bs.modal', function() {

		jQuery( 'html' ).css( 'overflow', '' );
		if ( 0 !== scrollbarWidth ) {
			if ( jQuery( 'body' ).hasClass( 'layout-boxed-mode' ) ) {
				jQuery( '#sliders-container .main-flex[data-parallax="1"]' ).css( 'margin-left', function( index, curValue ) {
					return parseFloat( curValue ) + ( scrollbarWidth / 2 )  + 'px';
				} );
			}
			jQuery( 'body, .fusion-is-sticky .fusion-header, .fusion-is-sticky .fusion-secondary-main-menu, #sliders-container .main-flex[data-parallax="1"], #wpadminbar, .fusion-footer.fusion-footer-parallax' ).css( 'padding-right', '' );
		}
	} );

	jQuery( '.fusion-modal' ).bind( 'show.bs.modal', function() {
		var modalWindow,
			fixedSelectors =  'body, .fusion-is-sticky .fusion-header, .fusion-is-sticky .fusion-secondary-main-menu, #sliders-container .main-flex[data-parallax="1"], #wpadminbar, .fusion-footer.fusion-footer-parallax';

		jQuery( 'html' ).css( 'overflow', 'visible' );
		if ( 0 !== scrollbarWidth ) {
			if ( jQuery( 'body' ).hasClass( 'layout-boxed-mode' ) ) {
				fixedSelectors =  'body, #wpadminbar';
				jQuery( '#sliders-container .main-flex[data-parallax="1"]' ).css( 'margin-left', function( index, curValue ) {
					return parseFloat( curValue ) - ( scrollbarWidth / 2 )  + 'px';
				} );
			}
			jQuery( fixedSelectors ).css( 'padding-right', function( index, curValue ) {
				return parseFloat( curValue ) + scrollbarWidth  + 'px';
			} );
		}
		modalWindow = jQuery( this );

		// Reinitialize dynamic content.
		setTimeout( function() {

			// Autoplay youtube videos, if the params have been set accordingly in the video shortcodes.
			modalWindow.find( '.fusion-youtube' ).find( 'iframe' ).each( function() {

				var func;
				if ( 1 === jQuery( this ).parents( '.fusion-video' ).data( 'autoplay' ) || 'true' === jQuery( this ).parents( '.fusion-video' ).data( 'autoplay' ) ) {
					jQuery( this ).parents( '.fusion-video' ).data( 'autoplay', 'false' );

					func = 'playVideo';
					this.contentWindow.postMessage( '{"event":"command","func":"' + func + '","args":""}', '*' );
				}
			} );

			// Autoplay vimeo videos, if the params have been set accordingly in the video shortcodes.
			modalWindow.find( '.fusion-vimeo' ).find( 'iframe' ).each( function() {
				if ( 1 === jQuery( this ).parents( '.fusion-video' ).data( 'autoplay' ) || 'true' === jQuery( this ).parents( '.fusion-video' ).data( 'autoplay' ) ) {
					jQuery( this ).parents( '.fusion-video' ).data( 'autoplay', 'false' );

					new Vimeo.Player( jQuery( this )[0] ).play();
				}
			} );

			// Google maps.
			if ( 'function' === typeof jQuery.fn.reinitializeGoogleMap ) {
				modalWindow.find( '.shortcode-map' ).each( function() {
					jQuery( this ).reinitializeGoogleMap();
				} );
			}

			// Image Carousels.
			if ( modalWindow.find( '.fusion-carousel' ).length && 'function' === typeof generateCarousel ) {
				generateCarousel();
			}

			// Portfolio.
			modalWindow.find( '.fusion-portfolio' ).each( function() {
				jQuery( this ).find( '.fusion-portfolio-wrapper' ).isotope();
			} );

			// Gallery.
			modalWindow.find( '.fusion-gallery' ).each( function() {
				jQuery( this ).isotope();
			} );

			// Flip Boxes.
			if ( 'function' === typeof jQuery.fn.fusionCalcFlipBoxesHeight ) {
				modalWindow.find( '.flip-box-inner-wrapper' ).each( function() {
					jQuery( this ).fusionCalcFlipBoxesHeight();
				} );
			}

			// Columns.
			if ( 'function' === typeof jQuery.fn.equalHeights ) {
				modalWindow.find( '.fusion-fullwidth.fusion-equal-height-columns' ).each( function() {
					jQuery( this ).find( '.fusion-layout-column .fusion-column-wrapper' ).equalHeights();
				} );
			}

			// Make WooCommerce shortcodes work.
			modalWindow.find( '.crossfade-images' ).each(	function() {
				fusionResizeCrossfadeImagesContainer( jQuery( this ) );
				fusionResizeCrossfadeImages( jQuery( this ) );
			} );

			// Blog.
			modalWindow.find( '.fusion-blog-shortcode' ).each( function() {
				jQuery( this ).find( '.fusion-blog-layout-grid' ).isotope();
			} );

			// Testimonials.
			modalWindow.find( '.fusion-testimonials .reviews' ).each( function() {
				jQuery( this ).css( 'height', jQuery( this ).children( '.active-testimonial' ).height() );
			} );

			// Select arrows.
			if ( 'function' === typeof calcSelectArrowDimensions ) {
				calcSelectArrowDimensions();
			}

			// Make premium sliders, other elements and nicescroll work.
			jQuery( window ).trigger( 'resize', [ 'modal-open' ] );
		}, 350 );
	} );

	if ( 1 == jQuery( '#sliders-container .tfs-slider' ).data( 'parallax' ) ) { // jshint ignore:line
		jQuery( '.fusion-modal' ).css( 'top', jQuery( '.header-wrapper' ).height() );
	}

	// Stop videos in modals when closed
	jQuery( '.fusion-modal' ).each( function() {
		jQuery( this ).on( 'hide.bs.modal', function() {

			// Youtube
			jQuery( this ).find( '.fusion-youtube iframe' ).each( function() {
				var func = 'pauseVideo';
				this.contentWindow.postMessage( '{"event":"command","func":"' + func + '","args":""}', '*' );
			} );

			// Vimeo
			jQuery( this ).find( '.fusion-vimeo iframe' ).each( function() {
				new Vimeo.Player( this ).pause();
			} );
		} );
	} );

	jQuery( '[data-toggle=modal]' ).on( 'click', function( e ) {
		e.preventDefault();
	} );

	jQuery( '.fusion-modal-text-link' ).click( function( e ) {
		e.preventDefault();
	} );
} );

jQuery( document ).ready( function() {
  jQuery( '.fusion-modal' ).on( 'shown.bs.modal', function() {
	var modalWindow = jQuery( this );
	modalWindow.find( '.shortcode-map' ).each( function() {
		jQuery( this ).reinitializeGoogleMap();
	} );
  } );
} );
