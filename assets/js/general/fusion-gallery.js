/* global imagesLoaded */

jQuery( window ).load( function() {
	jQuery( '.fusion-gallery-layout-grid, .fusion-gallery-layout-masonry' ).each( function() {
		var masonryGalleryContainer = jQuery( this ),
			galleryElements;

		galleryElements = masonryGalleryContainer.find( '.fusion-gallery-column' );
		masonryGalleryContainer.css( 'min-height', '500px' );
		galleryElements.hide();

		if ( jQuery( this ).hasClass( 'fusion-gallery-layout-masonry' ) && ! jQuery( this ).hasClass( 'fusion-masonry-has-vertical' ) && 0 < jQuery( this ).find( '.fusion-grid-column:not(.fusion-grid-sizer)' ).not( '.fusion-element-landscape' ).length ) {
			jQuery( this ).addClass( 'fusion-masonry-has-vertical' );
		}

		imagesLoaded( galleryElements, function() {

			masonryGalleryContainer.css( 'min-height', '' );
			galleryElements.fadeIn();

			// Start isotope.
			masonryGalleryContainer.isotope( {
				layoutMode: 'packery',
				itemSelector: '.fusion-gallery-column',
				isOriginLeft: jQuery( 'body.rtl' ).length ? false : true,
				resizable: true,
				initLayout: false
			} );

			masonryGalleryContainer.on( 'layoutComplete', function( event, laidOutItems ) { // jshint ignore: line
				var gallery = jQuery( event.target );

				// Relayout the wrapping blog isotope grid, if it exists.
				if ( gallery.parents( '.fusion-blog-layout-grid' ).length && ! gallery.parents( '.fusion-blog-layout-grid' ).hasClass( 'fusion-blog-equal-heights' ) ) {
					setTimeout( function() {
						gallery.parents( '.fusion-blog-layout-grid' ).isotope();
					}, 50 );
				}

				// Relayout the wrapping portfolio isotope grid, if it exists.
				if ( gallery.parents( '.fusion-portfolio-wrapper' ).length ) {
					setTimeout( function() {
						gallery.parents( '.fusion-portfolio-wrapper' ).isotope();
					}, 50 );
				}
			} );

			masonryGalleryContainer.isotope();

			jQuery( window ).trigger( 'resize' );

			// Refresh the scrollspy script for one page layouts.
			jQuery( '[data-spy="scroll"]' ).each( function() {
				var $spy = jQuery( this ).scrollspy( 'refresh' ); // jshint ignore:line
			} );
		} );
	} );
} );
