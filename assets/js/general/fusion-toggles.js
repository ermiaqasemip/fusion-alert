/* global generateCarousel, fusionResizeCrossfadeImagesContainer, fusionResizeCrossfadeImages, calcSelectArrowDimensions */
jQuery( window ).load( function() {

	// Boxed toggles should be clickable everywhere.
	jQuery( '.fusion-toggle-boxed-mode .panel-collapse' ).on( 'click', function( e ) {
		if ( ! jQuery( e.target ).is( 'a' ) && ! jQuery( e.target ).is( 'button' ) && ! jQuery( e.target ).hasClass( 'fusion-button-text' ) ) {
			jQuery( this ).parents( '.fusion-panel' ).find( '.panel-title > a' ).trigger( 'click' );
		}
	} );

	window.fusionAccordianClick = false;

	// Toggles.
	jQuery( document ).on( 'click dblclick', '.fusion-accordian .panel-title a', function( e ) {

		var clickedToggle,
			toggleContentToActivate,
			toggleChildren;

		e.preventDefault();

		// Make sure nothing happens, while one toggle is still transitioning.
		if ( jQuery( this ).parents( '.fusion-accordian' ).find( '.toggle-fadein' ).length && jQuery( this ).parents( '.fusion-accordian' ).find( '.toggle-fadein' )[0] !== jQuery( this ).parents( '.fusion-panel' ).find( '.panel-collapse' )[0] ) {
			return;
		}

		if ( true === window.fusionAccordianClick ) {
			return;
		} else {
			window.fusionAccordianClick = true;
		}

		clickedToggle = jQuery( this );
		toggleContentToActivate = jQuery( jQuery( this ).data( 'target' ) ).find( '.panel-body' );
		toggleChildren = clickedToggle.parents( '.fusion-accordian' ).find( '.panel-title a' );

		if ( clickedToggle.hasClass( 'collapsed' ) ) {
			if ( 'undefined' !== typeof clickedToggle.data( 'parent' ) ) {
				toggleChildren.removeClass( 'active' );
			} else {
				clickedToggle.removeClass( 'active' );
			}

			// Make equal heights work.
			if ( clickedToggle.closest( '.fusion-fullwidth' ).hasClass( 'fusion-equal-height-columns' ) ) {
				setTimeout( function() {
					jQuery( window ).trigger( 'resize' );
				}, 350 );
			}
		} else {
			if ( 'undefined' !== typeof clickedToggle.data( 'parent' ) ) {
				toggleChildren.removeClass( 'active' );
			}
			clickedToggle.addClass( 'active' );

			// Reinitialize dynamic content.
			setTimeout( function() {

				// Google maps.
				if ( 'function' === typeof jQuery.fn.reinitializeGoogleMap ) {
					toggleContentToActivate.find( '.shortcode-map' ).each( function() {
						jQuery( this ).reinitializeGoogleMap();
					} );
				}

				// Image Carousels.
				if ( toggleContentToActivate.find( '.fusion-carousel' ).length && 'function' === typeof generateCarousel ) {
					generateCarousel();
				}

				// Portfolio.
				toggleContentToActivate.find( '.fusion-portfolio' ).each( function() {
					var $portfolioWrapper   = jQuery( this ).find( '.fusion-portfolio-wrapper' ),
						$portfolioWrapperID = $portfolioWrapper.attr( 'id' );

					// Done for multiple instances of portfolio shortcode. Isotope needs ids to distinguish between instances.
					if ( $portfolioWrapperID ) {
						$portfolioWrapper = jQuery( '#' + $portfolioWrapperID );
					}

					$portfolioWrapper.isotope();
				} );

				// Gallery.
				toggleContentToActivate.find( '.fusion-gallery' ).each( function() {
					jQuery( this ).isotope();
				} );

				// Flip Boxes.
				if ( 'function' === typeof jQuery.fn.fusionCalcFlipBoxesHeight ) {
					toggleContentToActivate.find( '.flip-box-inner-wrapper' ).each( function() {
						jQuery( this ).fusionCalcFlipBoxesHeight();
					} );
				}

				// Columns.
				if ( 'function' === typeof jQuery.fn.equalHeights ) {
					toggleContentToActivate.find( '.fusion-fullwidth.fusion-equal-height-columns' ).each( function() {
						jQuery( this ).find( '.fusion-layout-column .fusion-column-wrapper' ).equalHeights();
					} );
				}

				// Make WooCommerce shortcodes work.
				toggleContentToActivate.find( '.crossfade-images' ).each(	function() {
					fusionResizeCrossfadeImagesContainer( jQuery( this ) );
					fusionResizeCrossfadeImages( jQuery( this ) );
				} );

				// Blog.
				toggleContentToActivate.find( '.fusion-blog-shortcode' ).each( function() {
					jQuery( this ).find( '.fusion-blog-layout-grid' ).isotope();
				} );

				// Testimonials.
				toggleContentToActivate.find( '.fusion-testimonials .reviews' ).each( function() {
					jQuery( this ).css( 'height', jQuery( this ).children( '.active-testimonial' ).height() );
				} );

				// Select arrows.
				if ( 'function' === typeof calcSelectArrowDimensions ) {
					calcSelectArrowDimensions();
				}

				// Make premium sliders, other elements and nicescroll work.
				jQuery( window ).trigger( 'resize' );
			}, 350 );
		}

		window.fusionAccordianClick = false;

	} );
} );

jQuery( document ).ready( function() {
	jQuery( '.fusion-accordian .panel-title a' ).click( function( e ) {
		e.preventDefault();
	} );
} );
