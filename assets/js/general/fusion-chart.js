( function( jQuery ) {
	'use strict';

	var chart,
		chartDataElems,
		chartTitle,
		chartType,
		chartBorderSize,
		chartBorderType,
		chartFill,
		valuesArr,
		showTooltips,
		dataSetValues,
		label,
		legendLabels,
		legendPosition,
		bgColors,
		bgColor,
		borderColors,
		borderColor,
		axisTextColor,
		gridlineColor,
		xAxisLabel,
		yAxisLabel,
		pointStyle,
		pointRadius,
		pointBorderColor,
		pointBackgroundColor,
		chartData = {},
		chartOptions,
		maxDatasetCount,
		maxValueCount,
		cnt,
		i,
		isPastryChart,
		isRTL;

	jQuery.each( jQuery( '.fusion-chart' ), function( i, elem ) {

		// Read options for each chart and set defaults.
		chartType            = 'undefined' !== typeof jQuery( this ).data( 'type' ) ? jQuery( this ).data( 'type' ) : 'line';
		chartBorderSize      = 'undefined' !== typeof jQuery( this ).data( 'border_size' ) ? jQuery( this ).data( 'border_size' ) : 1;
		chartBorderType      = 'undefined' !== typeof jQuery( this ).data( 'border_type' ) ? jQuery( this ).data( 'border_type' ) : 'smooth';
		chartFill            = 'undefined' !== typeof jQuery( this ).data( 'chart_fill' ) ? jQuery( this ).data( 'chart_fill' ) : 'yes';
		xAxisLabel           = 'undefined' !== typeof jQuery( this ).data( 'x_axis_label' ) ? jQuery( this ).data( 'x_axis_label' ) : '';
		yAxisLabel           = 'undefined' !== typeof jQuery( this ).data( 'y_axis_label' ) ? jQuery( this ).data( 'y_axis_label' ) : '';
		valuesArr            = [],
		showTooltips         = 'undefined' !== typeof jQuery( this ).data( 'show_tooltips' ) ? jQuery( this ).data( 'show_tooltips' ) : 'yes';
		bgColors             = [],
		bgColor              = '',
		borderColors         = [],
		borderColor          = '',
		axisTextColor        = 'undefined' !== typeof jQuery( this ).data( 'chart_axis_text_color' ) ? jQuery( this ).data( 'chart_axis_text_color' ) : null;
		gridlineColor        = 'undefined' !== typeof jQuery( this ).data( 'chart_gridline_color' ) ? jQuery( this ).data( 'chart_gridline_color' ) : null;
		maxDatasetCount      = 0,
		maxValueCount        = 0,
		legendLabels         = [],
		legendPosition       = 'undefined' !== typeof jQuery( this ).data( 'chart_legend_position' ) ? jQuery( this ).data( 'chart_legend_position' ) : 'off';
		pointStyle           = 'undefined' !== typeof jQuery( this ).data( 'chart_point_style' ) ? jQuery( this ).data( 'chart_point_style' ) : '';
		pointRadius          = 'undefined' !== typeof jQuery( this ).data( 'chart_point_size' ) ? jQuery( this ).data( 'chart_point_size' ) : '';
		pointBorderColor     = 'undefined' !== typeof jQuery( this ).data( 'chart_point_border_color' ) ? jQuery( this ).data( 'chart_point_border_color' ) : '';
		pointBackgroundColor = 'undefined' !== typeof jQuery( this ).data( 'chart_point_bg_color' ) ? jQuery( this ).data( 'chart_point_bg_color' ) : '';
		isPastryChart        = false,
		isRTL                = jQuery( 'body' ).hasClass( 'rtl' ),
		chartData            = {
			labels: [],
			datasets: []
		},
		chartOptions = {
			responsive: true,
			tooltips: {
				enabled: true
			},
			scales: {
				xAxes: [ {
					display: false
				} ],
				yAxes: [ {
					display: false
				} ]
			},
			legend: {
				labels: {},
				display: true
			},
			layout: {

				// Padding is needed to prevent chart clipping when axes are missing.
				padding: {
					left: 5,
					right: 5,
					top: 5,
					bottom: 5
				}
			}
		};

		chartDataElems  = jQuery( this ).find( '.fusion-chart-dataset' );
		maxDatasetCount = jQuery( chartDataElems ).length;

		if ( 0 === maxDatasetCount ) {
			return;
		}

		if ( 5 < pointRadius || 5 < chartBorderSize ) {
			chartOptions.layout.padding.top = pointRadius < chartBorderSize ? chartBorderSize : pointRadius;
		}

		if ( 'polarArea' === chartType || 'radar' === chartType ) {
			chartOptions.scale = {};
		}

		isPastryChart = ( ( 'pie' === chartType || 'doughnut' === chartType || 'polarArea' === chartType ) || ( ( 'bar' === chartType || 'horizontalBar' === chartType ) && 1 === chartDataElems.length ) ) ? true : false;

		// For pastry charts we fetch this settings from parent shortcode element. For other chart types we pick that from child elements.
		if ( isPastryChart ) {
			if ( 'undefined' !== typeof jQuery( this ).data( 'bg_colors' ) ) {
				bgColors = jQuery( this ).data( 'bg_colors' ).toString().split( '|' );
			}

			if ( 'undefined' !== typeof jQuery( this ).data( 'border_colors' ) ) {
				borderColors = jQuery( this ).data( 'border_colors' ).toString().split( '|' );
			}

			if ( 'undefined' !== typeof jQuery( this ).data( 'x_axis_labels' ) ) {
				legendLabels = jQuery( this ).data( 'x_axis_labels' ).toString().split( '|' );
			}
		}

		if ( 'off' === chartFill ) {
			chartFill = false;
		}

		chartOptions.tooltips.enabled = 'yes' === showTooltips ? true : false;

		// Iterate through all child elements to get data.
		for ( cnt = 0; cnt < maxDatasetCount; cnt++ ) {
			label = '';

			if ( ! isPastryChart ) {
				if ( 'undefined' !== typeof jQuery( chartDataElems[ cnt ] ).data( 'label' ) ) {
					label = jQuery( chartDataElems[ cnt ] ).data( 'label' ).toString().trim();
					chartData.labels.push( label );

					legendLabels.push( label );
				}
			}

			if ( ! isPastryChart ) {
				if ( 'undefined' !== typeof jQuery( chartDataElems[ cnt ] ).data( 'background_color' ) ) {
					bgColors.push( jQuery( chartDataElems[ cnt ] ).data( 'background_color' ).toString().trim() );
				}

				if ( 'undefined' !== typeof jQuery( chartDataElems[ cnt ] ).data( 'border_color' ) ) {
					borderColors.push( jQuery( chartDataElems[ cnt ] ).data( 'border_color' ).toString().trim() );
				}
			}

			if ( 'undefined' !== typeof jQuery( chartDataElems[ cnt ] ).data( 'values' ) ) {
				valuesArr[ cnt ] = jQuery( chartDataElems[ cnt ] ).data( 'values' ).toString().split( '|' );

				if ( ! isPastryChart && maxValueCount < valuesArr[ cnt ].length ) {
					maxValueCount = valuesArr[ cnt ].length;
				}
			}
		}

		// Number of labels should match number of values (or chart is weird).
		if ( 'undefined' !== typeof jQuery( this ).data( 'x_axis_labels' ) ) {
			chartData.labels = jQuery( this ).data( 'x_axis_labels' ).toString().split( '|' );
		} else {
			if ( 'pie' === chartType || 'doughnut' === chartType || 'polarArea' === chartType ) {
				chartData.labels = new Array( chartDataElems.length );
			} else {
				chartData.labels = new Array( maxValueCount );
			}
		}

		if ( true === isRTL ) {
			chartData.labels = chartData.labels.reverse();
			bgColors = bgColors.reverse();
			borderColors = borderColors.reverse();
			legendLabels = legendLabels.reverse();

			for ( i = 0; i < valuesArr.length; i++ ) {
				valuesArr[i] = valuesArr[i].reverse();
			}
		}

		// Build chartJS datasets based on chart type.
		for ( cnt = 0; cnt < maxDatasetCount; cnt++ ) {
			dataSetValues = [];

			if ( isPastryChart ) {
				bgColor       = bgColors;
				borderColor   = borderColors;
				dataSetValues = valuesArr[ cnt ];
				label = 'undefined' !== typeof jQuery( chartDataElems[ cnt ] ).data( 'label' ) ? jQuery( chartDataElems[ cnt ] ).data( 'label' ).toString().trim() : '';
			} else {
				bgColor       = 'undefined' !== typeof bgColors[ cnt ] ? bgColors[ cnt ] : '';
				borderColor   = 'undefined' !== typeof borderColors[ cnt ] ? borderColors[ cnt ] : '';
				dataSetValues = valuesArr[ cnt ];
				label = 'undefined' !== typeof legendLabels[ cnt ]  && true === Array.isArray( legendLabels ) ? legendLabels[ cnt ] : '';
			}

			chartData.datasets.push( {
				label: label,
				data: dataSetValues,
				backgroundColor: bgColor,
				borderColor: borderColor,
				borderWidth: chartBorderSize
			} );

			if ( 'line' === chartType ) {
				chartData.datasets[ cnt ].fill = chartFill;

				if ( 'stepped' === chartBorderType ) {
					chartData.datasets[ cnt ].steppedLine = true;
				}

				if ( '' !== pointStyle ) {
						chartData.datasets[ cnt ].pointStyle = pointStyle;
				}

				if ( '' !== pointRadius ) {
						chartData.datasets[ cnt ].pointRadius      = pointRadius;
						chartData.datasets[ cnt ].pointHoverRadius = pointRadius;
				}

				if ( '' !== pointBorderColor ) {
						chartData.datasets[ cnt ].pointBorderColor = pointBorderColor;
				}

				if ( '' !== pointBackgroundColor ) {
						chartData.datasets[ cnt ].pointBackgroundColor = pointBackgroundColor;
				}
			}
		}

		// Needed for pastry charts.
		if ( isPastryChart ) {
			chartData.labels = legendLabels;
		}

		// Start Y axis at 0 for this charts.
		if ( 'bar' === chartType || 'line' === chartType ) {
			chartOptions.scales.yAxes[0].ticks = {
					beginAtZero: true
				};
		}

		// And this one as well.
		if ( 'horizontalBar' === chartType ) {
			chartOptions.scales.xAxes[0].ticks = {
					beginAtZero: true
				};
		}

		// We don't display axes for this chart types in any case.
		if ( '' !== xAxisLabel && 'pie' !== chartType && 'doughnut' !== chartType && 'polarArea' !== chartType && 'radar' !== chartType ) {
			chartOptions.scales.xAxes[0].display = true;
			chartOptions.scales.xAxes[0].scaleLabel = {
				display: true,
				labelString: xAxisLabel
			};

			if ( null !== axisTextColor ) {
				chartOptions.scales.xAxes[0].scaleLabel.fontColor = axisTextColor;
			}
		}

		// We don't display axes for this chart types in any case.
		if ( '' !== yAxisLabel && 'pie' !== chartType && 'doughnut' !== chartType && 'polarArea' !== chartType && 'radar' !== chartType ) {
			chartOptions.scales.yAxes[0].display = true;
			chartOptions.scales.yAxes[0].scaleLabel = {
					display: true,
					labelString: yAxisLabel
				};

				if ( null !== axisTextColor ) {
					chartOptions.scales.yAxes[0].scaleLabel.fontColor = axisTextColor;
				}

				if ( true === isRTL && 'horizontalBar' !== chartType ) {
					chartOptions.scales.yAxes[0].position = 'right';
				}
		}

		if ( 'line' === chartType && 'non_smooth' === chartBorderType ) {
			chartOptions.elements = {
				line: {
					tension: 0.000001
				}
			};
		}

		// We have custom legend.
		chartOptions.legend.display = false;

		// Define custom legend generator function.
		chartOptions.legendCallback = function( chart ) {
			var text = [],
				i,
				bgColor;

			text.push( '<ul class="fusion-chart-legend-' + chart.id + '">' );

			for ( i = 0; i < legendLabels.length; i++ ) {
				bgColor = 'undefined' !== typeof bgColors[ i ] ? bgColors[ i ] : 'transparent';
				text.push( '<li><span style="background-color:' + bgColor + '">' );
				if ( legendLabels[ i ] ) {
					text.push( legendLabels[ i ] );
				}
				text.push( '</span></li>' );
			}
			text.push( '</ul>' );

			return text.join( '' );
		};

		if ( null !== gridlineColor ) {

			if ( 'polarArea' === chartType || 'radar' === chartType ) {
				if ( 'undefined' === typeof chartOptions.scale.gridLines ) {
					chartOptions.scale.gridLines  = {};
					chartOptions.scale.angleLines = {};
				}

				chartOptions.scale.gridLines.color  = gridlineColor;
				chartOptions.scale.angleLines.color = gridlineColor;
			} else {
				if ( 'undefined' === typeof chartOptions.scales.yAxes[0].gridLines ) {
					chartOptions.scales.yAxes[0].gridLines = {};
				}

				if ( 'undefined' === typeof chartOptions.scales.xAxes[0].gridLines ) {
					chartOptions.scales.xAxes[0].gridLines = {};
				}

				chartOptions.scales.xAxes[0].gridLines.color = gridlineColor;
				chartOptions.scales.yAxes[0].gridLines.color = gridlineColor;
			}

		}

		if ( null !== axisTextColor ) {

			if ( 'polarArea' === chartType || 'radar' === chartType ) {
				if ( 'undefined' === typeof chartOptions.scale.ticks ) {
					chartOptions.scale.ticks = {};
				}

				chartOptions.scale.ticks.fontColor = axisTextColor;
			} else {
				if ( 'undefined' === typeof chartOptions.scales.yAxes[0].ticks ) {
					chartOptions.scales.yAxes[0].ticks = {};
				}

				if ( 'undefined' === typeof chartOptions.scales.xAxes[0].ticks ) {
					chartOptions.scales.xAxes[0].ticks = {};
				}

				chartOptions.scales.xAxes[0].ticks.fontColor = axisTextColor;
				chartOptions.scales.yAxes[0].ticks.fontColor = axisTextColor;
			}
		}

		// Generate chart.
		chart = new Chart( jQuery( this ).find( 'canvas' ).get( 0 ), {
			type: chartType,
			data: chartData,
			options: chartOptions
		} );

		// Generate legend if needed.
		if ( 'off' !== legendPosition ) {
			jQuery( this ).find( '.fusion-chart-legend-wrap' ).html( chart.generateLegend() );
		}

	} );
}( jQuery ) );
