/* global Modernizr, fusionContainerVars, getStickyHeaderHeight */
jQuery( window ).load( function() {
	jQuery( '.fullwidth-faded' ).fusionScroller( { type: 'fading_blur' } );
} );

jQuery( document ).ready( function() {
	if ( Modernizr.mq( 'only screen and (max-width: ' + fusionContainerVars.content_break_point + 'px)' ) ) {
		jQuery( '.fullwidth-faded' ).each( function() {
			var bkgdImg   = jQuery( this ).css( 'background-image' ),
				bkgdColor = jQuery( this ).css( 'background-color' );

			jQuery( this ).parent().css( 'background-image', bkgdImg );
			jQuery( this ).parent().css( 'background-color', bkgdColor );
			jQuery( this ).remove();
		} );
	}
} );

jQuery( window ).load( function() {

	// Check if page is already loaded scrolled, without anchor scroll script. If so, move to beginning of correct scrolling section.
	if ( jQuery( '#content' ).find( '.fusion-scroll-section' ).length && 'undefined' === typeof jQuery( '.fusion-page-load-link' ).attr( 'href' ) ) {
		setTimeout( function() {
			scrollToCurrentScrollSection();
		}, 400 );
	}
} );

jQuery( document ).ready( function() {
	var mainContentScrollSections = jQuery( '#content' ).find( '.fusion-scroll-section' ),
		lastYPosition,
		onlyOneScrollSectionOnPage,
		stickyHeaderHeight = ( ! Boolean( Number( fusionContainerVars.is_sticky_header_transparent ) ) && 'function' === typeof getStickyHeaderHeight ) ? getStickyHeaderHeight( true ) : 0,
		adminbarHeight = ( jQuery( '#wpadminbar' ).length ) ? parseInt( jQuery( '#wpadminbar' ).height(), 10 ) : 0,
		sectionTopOffset = stickyHeaderHeight + adminbarHeight,
		overallSectionHeight;

	// Check if there are 100% height scroll sections.
	if ( mainContentScrollSections.length ) {
		if ( ! jQuery( '#content' ).find( '.non-hundred-percent-height-scrolling' ).length && 1 === mainContentScrollSections.length && ! jQuery.trim( jQuery( '#sliders-container' ).html() ) ) {
			mainContentScrollSections.addClass( 'active' );
			mainContentScrollSections.find( '.fusion-scroll-section-nav li:first a' ).addClass( 'active' );
			onlyOneScrollSectionOnPage = true;
		}

		// Set correct heights for the wrapping scroll sections and individual scroll elements, based on the number of elements, sticky header and admin bar.
		mainContentScrollSections.each( function() {
			if ( 1 < jQuery( this ).children( 'div' ).length ) {
				overallSectionHeight = ( sectionTopOffset ) ? 'calc(' + ( jQuery( this ).children( 'div' ).size() * 100 + 50 ) + 'vh - ' + sectionTopOffset + 'px)' : ( jQuery( this ).children( 'div' ).size() * 100 + 50 ) + 'vh';

				// Set correct height for the wrapping scroll section.
				jQuery( this ).css( 'height', overallSectionHeight );

				// Set the correct height offset per section and also for the nav.
				if ( sectionTopOffset ) {
					jQuery( this ).find( '.hundred-percent-height-scrolling' ).css( 'height', 'calc(100vh - ' + sectionTopOffset + 'px)' );

					jQuery( this ).find( '.fusion-scroll-section-nav' ).css( 'top', 'calc(50% + ' + ( sectionTopOffset / 2 ) + 'px)' );
				}
			}
		} );

		// Set the last scroll position to the initial loading value.
		lastYPosition = jQuery( window ).scrollTop();

		jQuery( window ).scroll( function() {
			var currentYPosition = jQuery( window ).scrollTop();

			// Position the elements of a scroll section correctly.
			jQuery( '.fusion-scroll-section' ).each( function() {
				if ( 1 < jQuery( this ).children( 'div' ).length && ! jQuery( this ).hasClass( 'fusion-scroll-section-mobile-disabled' ) ) {
					jQuery( this ).fusionPositionScrollSectionElements( lastYPosition, currentYPosition, onlyOneScrollSectionOnPage );
				}
			} );

			lastYPosition = currentYPosition;
		} );

		// Set the "active" class to the correct scroll nav link and the correct section element.
		jQuery( '.fusion-scroll-section-link' ).on( 'click', function( e ) {
			var scrollSection = jQuery( this ).parents( '.fusion-scroll-section' ),
				numberOfLastActive = parseInt( jQuery( this ).parents( '.fusion-scroll-section-nav' ).find( '.fusion-scroll-section-link.active' ).data( 'element' ), 10 ),
				numberOfNewActive = parseInt( jQuery( this ).data( 'element' ), 10 ),
				numberOfScrolledElements = Math.abs( numberOfNewActive - numberOfLastActive ),
				scrollAnimationTime = ( 350 + 30 * ( numberOfScrolledElements - 1 ) ) * numberOfScrolledElements;

			e.preventDefault();

			// If slide is already active, do nothing.
			if ( 0 === numberOfScrolledElements ) {
				return;
			}

			if ( 20 < numberOfScrolledElements ) {
				scrollAnimationTime = ( 350 + 30 * 20 ) * numberOfScrolledElements;
			}

			// Add active class to the correct section element.
			jQuery( this ).parents( '.fusion-scroll-section' ).find( '.fusion-scroll-section-element' ).removeClass( 'active' );

			// Scroll to correct position of current section.
			jQuery( 'html, body' ).animate( {
				scrollTop: Math.ceil( scrollSection.offset().top ) + jQuery( window ).height() * ( jQuery( this ).data( 'element' ) - 1 )
			}, scrollAnimationTime, 'linear' );

		} );
	}

	if ( jQuery( '.hundred-percent-height' ).length ) {
		setCorrectResizeValuesForScrollSections();

		jQuery( window ).on( 'resize', function() {
			setCorrectResizeValuesForScrollSections();
		} );
	}
} );

function setCorrectResizeValuesForScrollSections() {

	var mainContentScrollSections = jQuery( '#content' ).find( '.fusion-scroll-section' ),
		stickyHeaderHeight = 0,
		stickyHeaderHeight = 0,
		sectionTopOffset   = 0,
		adminbarHeight     = 0;


	// Check if there are 100% height scroll sections.
	if ( mainContentScrollSections.length ) {

		// Resize the fixed containers, to fit the correct area.
		jQuery( '.fusion-scroll-section.active' ).find( '.fusion-scroll-section-element' ).css( {
			'left': jQuery( '#content' ).offset().left
		} );

		jQuery( '.fusion-scroll-section' ).find( '.fusion-scroll-section-element' ).css( {
			'width': jQuery( '#content' ).width()
		} );

		if ( 0 == fusionContainerVars.container_hundred_percent_height_mobile ) { // jshint ignore:line

			// Mobile view.
			if ( Modernizr.mq( 'only screen and (max-width: ' + fusionContainerVars.content_break_point + 'px)' ) ) {
				jQuery( '.fusion-scroll-section' ).removeClass( 'active' ).addClass( 'fusion-scroll-section-mobile-disabled' );
				jQuery( '.fusion-scroll-section' ).attr( 'style', '' );
				jQuery( '.fusion-scroll-section' ).find( '.fusion-scroll-section-element' ).attr( 'style', '' );
				jQuery( '.fusion-scroll-section' ).find( '.hundred-percent-height-scrolling' ).css( 'height', 'auto' );
				jQuery( '.fusion-scroll-section' ).find( '.fusion-fullwidth-center-content' ).css( 'height', 'auto' );

			} else {

				// Desktop view.
				if ( jQuery( '.fusion-scroll-section' ).hasClass( 'fusion-scroll-section-mobile-disabled' ) ) {

					jQuery( '.fusion-scroll-section' ).find( '.fusion-fullwidth-center-content' ).css( 'height', '' );

					if ( ! Boolean( Number( fusionContainerVars.is_sticky_header_transparent ) ) && 'function' === typeof getStickyHeaderHeight ) {
						stickyHeaderHeight = getStickyHeaderHeight( true );
					}
					if ( jQuery( '#wpadminbar' ).length ) {
						adminbarHeight = parseInt( jQuery( '#wpadminbar' ).height(), 10 );
					}
					sectionTopOffset = stickyHeaderHeight + adminbarHeight;

					// Set correct heights for the wrapping scroll sections, based on the number of elements.
					mainContentScrollSections.each( function() {
						if ( 1 < jQuery( this ).children( 'div' ).length ) {
							jQuery( this ).css( 'height', ( jQuery( this ).children( 'div' ).size() * 100 + 50 ) + 'vh' );

							jQuery( this ).find( '.hundred-percent-height-scrolling' ).css( 'height', 'calc(100vh - ' + sectionTopOffset + 'px)' );
						}
					} );

					scrollToCurrentScrollSection();
				}
			}
		}
	}

	// Special handling of 100% height containers without scrolling sections.
	if ( jQuery( '.hundred-percent-height.non-hundred-percent-height-scrolling' ).length ) {
		if ( ! Boolean( Number( fusionContainerVars.is_sticky_header_transparent ) ) && 'function' === typeof getStickyHeaderHeight ) {
			stickyHeaderHeight = getStickyHeaderHeight( true );
		}
		if ( jQuery( '#wpadminbar' ).length ) {
			adminbarHeight = parseInt( jQuery( '#wpadminbar' ).height(), 10 );
		}
		sectionTopOffset = stickyHeaderHeight + adminbarHeight;

		if ( 0 == fusionContainerVars.container_hundred_percent_height_mobile ) { // jshint ignore:line

			// Mobile view.
			if ( Modernizr.mq( 'only screen and (max-width: ' + fusionContainerVars.content_break_point + 'px)' ) ) {
				jQuery( '.hundred-percent-height.non-hundred-percent-height-scrolling' ).css( 'height', 'auto' );

				jQuery( '.hundred-percent-height.non-hundred-percent-height-scrolling' ).find( '.fusion-fullwidth-center-content' ).css( 'height', 'auto' );
			} else {

				// Desktop view.
				jQuery( '.hundred-percent-height.non-hundred-percent-height-scrolling' ).css( 'height', 'calc(100vh - ' + sectionTopOffset + 'px)' );

				jQuery( '.hundred-percent-height.non-hundred-percent-height-scrolling' ).find( '.fusion-fullwidth-center-content' ).css( 'height', '' );
			}
		}
	}
}

function scrollToCurrentScrollSection() {
	var currentScrollPosition = jQuery( window ).scrollTop(), // jshint ignore:line
		viewportTop           = Math.ceil( jQuery( window ).scrollTop() ),
		viewportHeight        = jQuery( window ).height(),
		viewportBottom        = Math.floor( viewportTop + viewportHeight ),
		stickyHeaderHeight    = ( ! Boolean( Number( fusionContainerVars.is_sticky_header_transparent ) ) && 'function' === typeof getStickyHeaderHeight ) ? getStickyHeaderHeight( true ) : 0,
		adminbarHeight        = ( jQuery( '#wpadminbar' ).length ) ? parseInt( jQuery( '#wpadminbar' ).height(), 10 ) : 0;

	viewportTop += stickyHeaderHeight + adminbarHeight;

	// Get the scroll section in view, but only if not while loading when a one page scroll link is present.
	if ( ! jQuery( '.fusion-page-load-link' ).hasClass( 'fusion-page.load-scroll-section-link' ) ) {
		jQuery( '.fusion-scroll-section' ).each( function() {
			var section       = jQuery( this ),
				sectionTop    = Math.ceil( section.offset().top ),
				sectionHeight = Math.ceil( section.outerHeight() ),
				sectionBottom = Math.floor( sectionTop + sectionHeight );

			// Scrolled position is inside a scroll section.
			if ( sectionTop <= viewportTop && sectionBottom >= viewportBottom ) {
				section.addClass( 'active' );

				// Scroll to beginning position of correct section.
				jQuery( 'html, body' ).animate( {
					scrollTop: sectionTop - 50
				}, { duration: 50, easing: 'easeInExpo', complete: function() {
					jQuery( 'html, body' ).animate( {
						scrollTop: sectionTop
					}, { duration: 50, easing: 'easeOutExpo', complete: function() {

						// Remove the mobile disabling class if in desktop mode.
						if ( ! Modernizr.mq( 'only screen and (max-width: ' + fusionContainerVars.content_break_point + 'px)' ) ) {
							jQuery( '.fusion-scroll-section' ).removeClass( 'fusion-scroll-section-mobile-disabled' );
						}
					}
					} );
				}
				} );
			}
		} );
	}
}

( function( jQuery ) {

	'use strict';

	jQuery.fn.fusionPositionScrollSectionElements = function( lastYPosition, currentYPosition, onlyOneScrollSectionOnPage ) {
		var section          = jQuery( this ),
			sectionTop       = Math.ceil( section.offset().top ),
			sectionHeight    = Math.ceil( section.outerHeight() ),
			sectionBottom    = Math.floor( sectionTop + sectionHeight ),
			viewportTop      = Math.ceil( jQuery( window ).scrollTop() ),
			viewportHeight   = jQuery( window ).height(),
			viewportBottom   = Math.floor( viewportTop + viewportHeight ),
			numberOfElements = section.find( '.fusion-scroll-section-element' ).length,
			currentSegment   = 0,
			sectionWidth,
			sectionTopOffset,
			sectionLeftOffset,
			sectionPadding,
			i;

		onlyOneScrollSectionOnPage = onlyOneScrollSectionOnPage || false;

		// Top offset is 0 or wpadminbar height if no sticky header is used.
		sectionTopOffset = ( jQuery( '#wpadminbar' ).length ) ? parseInt( jQuery( '#wpadminbar' ).height(), 10 ) : 0;

		// We have a non-transparent sticky header, so we need correct top offset.
		sectionTopOffset += ( ! Boolean( Number( fusionContainerVars.is_sticky_header_transparent ) ) && 'function' === typeof getStickyHeaderHeight ) ? getStickyHeaderHeight( true ) : 0;

		// Add the sticky header offset to the viewpot top for calcs.
		viewportTop += sectionTopOffset;

		// Set the correct offsets for general use.
		sectionWidth      = jQuery( '#content' ).width();
		sectionLeftOffset = jQuery( '#content' ).offset().left;
		sectionPadding    = '0';

		// Make sure there is more than one scroll section, otherwise the one will always be active.
		if ( ! onlyOneScrollSectionOnPage ) {

			// Set the section to active if it is in viewport.
			if ( sectionTop <= viewportTop && sectionBottom >= viewportBottom ) {
				section.addClass( 'active' );
			} else {
				section.removeClass( 'active' );
			}
		}

		// Scrolling down.
		if ( lastYPosition < currentYPosition ) {

			// Get the current element by checking in which section segment the viewport top is.
			for ( i = 1; i < numberOfElements; i++ ) {
				if ( viewportTop >= sectionTop + viewportHeight * i && viewportTop < sectionTop + viewportHeight * ( i + 1 ) ) {
					currentSegment = i + 1;
				}
			}

			// First element comes into viewport.
			if ( sectionTop <= viewportTop && sectionTop + viewportHeight > viewportTop ) {

				// Set correct element to be active.
				section.find( '.fusion-scroll-section-element' ).removeClass( 'active' );
				section.children( ':nth-child(1)' ).addClass( 'active' );

				// Set correct navigation link to be active.
				section.find( '.fusion-scroll-section-nav a' ).removeClass( 'active' );
				section.find( '.fusion-scroll-section-nav a[data-element="' + section.children( ':nth-child(1)' ).data( 'element' ) + '"] ' ).addClass( 'active' );

				// When entering a scroll section all elements need to be positioned fixed and other values set depending on layout.
				section.find( '.fusion-scroll-section-element' ).css( {
					'position': 'fixed',
					'top': sectionTopOffset,
					'left': sectionLeftOffset,
					'padding': '0 ' + sectionPadding,
					'width': sectionWidth
				} );

				section.children( ':nth-child(1)' ).css( 'display', 'block' );

			} else if ( sectionBottom <= viewportBottom && 'absolute' !== section.find( '.fusion-scroll-section-element' ).last().css( 'position' ) ) {

				// Last element is in viewport and it is scrolled further down, so exiting scroll section.

				section.find( '.fusion-scroll-section-element' ).removeClass( 'active' );
				section.find( '.fusion-scroll-section-element' ).last().addClass( 'active' );

				section.find( '.fusion-scroll-section-element' ).css( 'position', 'absolute' );

				section.find( '.fusion-scroll-section-element' ).last().css( {
					'top': 'auto',
					'left': '0',
					'bottom': '0',
					'padding': ''
				} );
			} else if ( 0 < currentSegment && ! section.children( ':nth-child(' + currentSegment + ')' ).hasClass( 'active' ) ) {

				// Transition between individual elements.

				// Set correct element to be active.
				section.find( '.fusion-scroll-section-element' ).removeClass( 'active' );
				section.children( ':nth-child(' + currentSegment + ')' ).addClass( 'active' );

				// Set correct navigation link to be active.
				section.find( '.fusion-scroll-section-nav a' ).removeClass( 'active' );
				section.find( '.fusion-scroll-section-nav a[data-element="' + section.children( ':nth-child(' + currentSegment + ')' ).data( 'element' ) + '"] ' ).addClass( 'active' );
			}
		} else if ( lastYPosition > currentYPosition ) {

			// Scrolling up.

			// Get the current element by checking in which section segment the viewport top is.
			for ( i = 1; i < numberOfElements; i++ ) {
				if ( sectionTop + viewportHeight * i > viewportTop && sectionTop + viewportHeight * ( i - 1 ) < viewportTop ) {
					currentSegment = i;
				}
			}

			// Entering the last element of a scrolling section.
			if ( sectionBottom >= viewportBottom && sectionTop + viewportHeight * ( numberOfElements - 1 ) < viewportTop && 'fixed' !== section.find( '.fusion-scroll-section-element' ).last().css( 'position' ) ) {

				// Set correct element to be active.
				section.find( '.fusion-scroll-section-element' ).removeClass( 'active' );
				section.find( '.fusion-scroll-section-element' ).last().addClass( 'active' );

				// Set correct navigation link to be active.
				section.find( '.fusion-scroll-section-nav a' ).removeClass( 'active' );
				section.find( '.fusion-scroll-section-nav a[data-element="' + section.find( '.fusion-scroll-section-element' ).last().data( 'element' ) + '"] ' ).addClass( 'active' );

				// When entering a scroll section all elements need to be positioned fixed and other values set depending on layout.
				section.find( '.fusion-scroll-section-element' ).css( {
					'position': 'fixed',
					'top': sectionTopOffset,
					'left': sectionLeftOffset,
					'padding': '0 ' + sectionPadding,
					'width': sectionWidth
				} );

				section.find( '.fusion-scroll-section-element' ).last().css( 'display', 'block' );

			} else if ( ( sectionTop >= viewportTop || ( 0 === jQuery( window ).scrollTop() && section.find( '.fusion-scroll-section-element' ).first().hasClass( 'active' ) ) ) && '' !== section.find( '.fusion-scroll-section-element' ).first().css( 'position' ) ) {

				// First element is in viewport and it is further scrolled up.

				section.find( '.fusion-scroll-section-element' ).removeClass( 'active' );
				section.find( '.fusion-scroll-section-element' ).first().addClass( 'active' );

				section.find( '.fusion-scroll-section-element' ).css( 'position', '' );

				section.find( '.fusion-scroll-section-element' ).first().css( 'padding', '' );

			} else if ( 0 < currentSegment && ! section.children( ':nth-child(' + currentSegment + ')' ).hasClass( 'active' ) ) {

				// Transition between individual elements.

				// Set correct element to be active.
				section.find( '.fusion-scroll-section-element' ).removeClass( 'active' );
				section.children( ':nth-child(' + currentSegment + ')' ).addClass( 'active' );

				// Set correct navigation link to be active.
				section.find( '.fusion-scroll-section-nav a' ).removeClass( 'active' );
				section.find( '.fusion-scroll-section-nav a[data-element="' + section.children( ':nth-child(' + currentSegment + ')' ).data( 'element' ) + '"] ' ).addClass( 'active' );
			}
		}
	};
} ( jQuery ) );
