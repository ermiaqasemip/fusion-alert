/* global fusionVideoVars, Vimeo */
jQuery( document ).ready( function() { // Start document_ready_1

	// Enable autoplaying videos when not in a modal
	jQuery( '.fusion-video' ).each( function() {
		if ( ! jQuery( this ).parents( '.fusion-modal' ).length && 1 == jQuery( this ).data( 'autoplay' ) && jQuery( this ).is( ':visible' ) ) { // jshint ignore:line
			jQuery( this ).find( 'iframe' ).each( function() {
				jQuery( this ).attr( 'src', jQuery( this ).attr( 'src' ).replace( 'autoplay=0', 'autoplay=1' ) );
			} );
		}
	} );

	// Video resize
	jQuery( window ).on( 'resize', function() {

		var iframes = document.querySelectorAll( 'iframe' ),
			player,
			i,
			length = iframes.length,
			func   = 'pauseVideo';

		// Stop autoplaying youtube video when not visible on resize
		jQuery( '.fusion-youtube' ).each( function() {
			if ( ! jQuery( this ).is( ':visible' ) && ( ! jQuery( this ).parents( '.fusion-modal' ).length || jQuery( this ).parents( '.fusion-modal' ).is( ':visible' ) ) ) {
				jQuery( this ).find( 'iframe' ).each( function() {
					this.contentWindow.postMessage( '{"event":"command","func":"' + func + '","args":""}', '*' );
				} );
			}
		} );

		// Stop autoplaying vimeo video when not visible on resize
		if ( 'undefined' !== typeof Vimeo ) {
			for ( i = 0; i < length; i++ ) {
				if ( -1 < iframes[i].src.toLowerCase().indexOf( 'vimeo' ) && ! jQuery( iframes[i] ).is( ':visible' ) && ( ! jQuery( iframes[i] ).data( 'privacy-src' ) || ! jQuery( iframes[i] ).hasClass( 'fusion-hidden' ) ) && ( ! jQuery( iframes[i] ).parents( '.fusion-modal' ).length || jQuery( iframes[i] ).parents( '.fusion-modal' ).is( ':visible' ) ) ) {
					player = new Vimeo.Player( iframes[i] );
					player.pause();
				}
			}
		}
	} );
} );
