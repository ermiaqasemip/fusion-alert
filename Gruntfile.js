module.exports = function( grunt ) {

	grunt.initConfig( {
		makepot: {
			target: {
				options: {
					type: 'wp-plugin',
					domainPath: 'languages'
				}
			}
		},

		addtextdomain: {
			options: {
				textdomain: 'fusion-builder',
				updateDomains: [
					'Avada',
					'fusionredux-framework',
					'fusion-library-textdomain',
					'fusion-core',
					'fusion-builder',
					'envato-market',
					'Fusion',
					'wordpress-importer',
					'fusion-library'
				]
			},
			target: {
				files: {
					src: [
						'inc/lib/*.php',
						'inc/lib/**/*.php',
						'inc/lib/**/**/*.php',
						'inc/lib/**/**/**/*.php'
					]
				}
			}
		},

		copy: {
			proxy: {
				src: 'languages/Fusion-Builder.pot',
				dest: 'languages/fb.pot'
			},
			final: {
				src: 'languages/fb.pot',
				dest: 'languages/fusion-builder.pot'
			}
		},

		clean: {
			'proxy': [ 'languages/Fusion-Builder.pot' ],
			'final': [ 'languages/fb.pot' ]
		},

		less: {
			options: {
				plugins: [ new( require( 'less-plugin-autoprefix' ) )( { browsers: [ 'last 10 versions' ] } ) ]
			},
			development: {
				files: {
					'css/elements-preview.css': 'less/elements-preview.less',
					'css/fusion-builder-admin.css': 'less/fusion-builder-admin.less',
					'css/fusion-builder.css': 'less/fusion-builder.less',
					'css/fusion-shortcodes.css': [ 'less/fusion-shortcodes.less', 'less/shortcodes/shortcodes.less', 'less/icomoon.less' ],
					'assets/css/animations.css': [ 'less/icon_animations.less', 'less/animations.less' ],
					'assets/css/ilightbox.css': 'less/plugins/iLightbox/iLightbox.less',
					'assets/css/nouislider.css': 'less/nouislider.less',
					'css/style.css': 'less/style.less'
				}
			}
		},

		csscomb: {
			target: {
				files: {
					'css/style.css': 'css/style.css',
					'css/fusion-builder-admin.css': 'css/fusion-builder-admin.css'
				}
			}
		},

		csslint: {
			target: {
				options: {
					csslintrc: '.csslintrc'
				},
				src: [ 'css/style.css' ]
			}
		},

		concat: {
			options: {
				separator: ';'
			},
			main: {
				src: [
					'js/util.js',
					'js/sticky-menu.js',
					'js/models/model-element.js',
					'js/models/model-view-manager.js',
					'js/collections/collection-element.js',
					'js/views/view-element.js',
					'js/views/view-element-preview.js',
					'js/views/view-elements-library.js',
					'js/views/view-generator-elements.js',
					'js/views/view-container.js',
					'js/views/view-blank-page.js',
					'js/views/view-row.js',
					'js/views/view-row-nested.js',
					'js/views/view-column-nested.js',
					'js/views/view-column.js',
					'js/views/view-modal.js',
					'js/views/view-next-page.js',
					'js/views/view-context-menu.js',
					'js/views/view-element-settings.js',
					'js/views/view-multi-element-child-settings.js',
					'js/views/view-multi-element-sortable-ui.js',
					'js/views/view-multi-element-sortable-child.js',
					'js/views/view-column-library.js',
					'js/views/view-nested-column-library.js',
					'js/app.js',
					'js/fusion-shortcode-generator.js',
					'js/fusion-history.js'
				],
				dest: 'js/fusion-builder.js'
			}
		},

		uglify: {
			main: {
				options: {
					mangle: true,
					compress: {
						sequences: true,
						dead_code: true,
						conditionals: true,
						booleans: true,
						unused: true,
						if_return: true,
						join_vars: true,
						drop_console: true
					}
				},
				files: {
					'js/fusion-builder.js': [ 'js/fusion-builder.js' ]
				}
			},
			general: {
				options: {
					mangle: true,
					compress: {
						sequences: true,
						dead_code: true,
						conditionals: true,
						booleans: true,
						unused: true,
						if_return: true,
						join_vars: true,
						drop_console: true
					}
				},
				files: [ {
					expand: true,
					cwd: 'assets/js/general',
					src: '*.js',
					dest: 'assets/js/min/general'
				} ]
			},
			library: {
				options: {
					mangle: true,
					compress: {
						sequences: true,
						dead_code: true,
						conditionals: true,
						booleans: true,
						unused: true,
						if_return: true,
						join_vars: true,
						drop_console: true
					}
				},
				files: [ {
					expand: true,
					cwd: 'assets/js/library',
					src: '*.js',
					dest: 'assets/js/min/library'
				} ]
			}
		},

		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'css/fusion-shortcodes.min.css': [ 'css/fusion-shortcodes.css' ],
					'inc/woocommerce/css/woocommerce.min.css': [ 'inc/woocommerce/css/woocommerce.css' ],
					'assets/css/animations.min.css': [ 'assets/css/animations.css' ],
					'assets/css/ilightbox.min.css': [ 'assets/css/ilightbox.css' ]
				}
			}
		},

		exec: {
			gitClone: 'rm -Rf inc/lib && git clone -b development git@github.com:Theme-Fusion/fusion-library inc/lib',
			gitPull: 'cd inc/lib && git pull origin master'
		},

		// Check JS syntax.
		jscs: {
			src: [ 'Gruntfile.js', 'js/*.js', 'js/**/*.js', 'assets/js/general/*.js', '!js/fusion-builder.js', '!js/wNumb.js', '!js/sticky-menu.js' ],
			options: {
				config: '.jscsrc'
			}
		},

		// JSHint.
		jshint: {
			all: [ 'Gruntfile.js', 'js/*.js', 'js/**/*.js', 'assets/js/general/*.js', '!js/fusion-builder.js', '!js/wNumb.js', '!js/sticky-menu.js' ]
		},

		watch: {
			css: {
				files: [ '**/*.less', '**/**/*.less' ],
				tasks: [ 'less:development', 'csscomb', 'cssmin' ]
			},
			js: {
				files: [ '**/*.js', '**/**/*.js' ],
				tasks: [ 'jshint', 'jscs', 'concat' ]
			}
		}
	} );

	grunt.loadNpmTasks( 'grunt-contrib-concat' );
	grunt.loadNpmTasks( 'grunt-contrib-uglify' );
	grunt.loadNpmTasks( 'grunt-contrib-less' );
	grunt.loadNpmTasks( 'grunt-csscomb' );
	grunt.loadNpmTasks( 'grunt-contrib-csslint' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-wp-i18n' );
	grunt.loadNpmTasks( 'grunt-contrib-copy' );
	grunt.loadNpmTasks( 'grunt-contrib-clean' );
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
	grunt.loadNpmTasks( 'grunt-exec' );
	grunt.loadNpmTasks( 'grunt-jscs' );
	grunt.loadNpmTasks( 'grunt-contrib-jshint' );

	grunt.registerTask( 'default', [ 'less', 'csscomb', 'cssmin', 'concat', 'exec:gitClone', 'addtextdomain', 'uglify:main', 'uglify:general', 'uglify:library', 'makepot', 'copy:proxy', 'clean:proxy', 'copy:final', 'clean:final' ] );
	grunt.registerTask( 'check', [ 'jshint', 'jscs' ] );
};
