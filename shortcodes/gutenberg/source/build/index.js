/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/helpers/defineProperty.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/defineProperty.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function _defineProperty(obj, key, value) {\n  if (key in obj) {\n    Object.defineProperty(obj, key, {\n      value: value,\n      enumerable: true,\n      configurable: true,\n      writable: true\n    });\n  } else {\n    obj[key] = value;\n  }\n\n  return obj;\n}\n\nmodule.exports = _defineProperty;\n\n//# sourceURL=webpack:///./node_modules/@babel/runtime/helpers/defineProperty.js?");

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/extends.js":
/*!********************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/extends.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function _extends() {\n  module.exports = _extends = Object.assign || function (target) {\n    for (var i = 1; i < arguments.length; i++) {\n      var source = arguments[i];\n\n      for (var key in source) {\n        if (Object.prototype.hasOwnProperty.call(source, key)) {\n          target[key] = source[key];\n        }\n      }\n    }\n\n    return target;\n  };\n\n  return _extends.apply(this, arguments);\n}\n\nmodule.exports = _extends;\n\n//# sourceURL=webpack:///./node_modules/@babel/runtime/helpers/extends.js?");

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/objectSpread.js":
/*!*************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/objectSpread.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var defineProperty = __webpack_require__(/*! ./defineProperty */ \"./node_modules/@babel/runtime/helpers/defineProperty.js\");\n\nfunction _objectSpread(target) {\n  for (var i = 1; i < arguments.length; i++) {\n    var source = arguments[i] != null ? arguments[i] : {};\n    var ownKeys = Object.keys(source);\n\n    if (typeof Object.getOwnPropertySymbols === 'function') {\n      ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {\n        return Object.getOwnPropertyDescriptor(source, sym).enumerable;\n      }));\n    }\n\n    ownKeys.forEach(function (key) {\n      defineProperty(target, key, source[key]);\n    });\n  }\n\n  return target;\n}\n\nmodule.exports = _objectSpread;\n\n//# sourceURL=webpack:///./node_modules/@babel/runtime/helpers/objectSpread.js?");

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/typeof.js":
/*!*******************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/typeof.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function _typeof2(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof2(obj); }\n\nfunction _typeof(obj) {\n  if (typeof Symbol === \"function\" && _typeof2(Symbol.iterator) === \"symbol\") {\n    module.exports = _typeof = function _typeof(obj) {\n      return _typeof2(obj);\n    };\n  } else {\n    module.exports = _typeof = function _typeof(obj) {\n      return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : _typeof2(obj);\n    };\n  }\n\n  return _typeof(obj);\n}\n\nmodule.exports = _typeof;\n\n//# sourceURL=webpack:///./node_modules/@babel/runtime/helpers/typeof.js?");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/assets/styles/main.css":
/*!**************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/assets/styles/main.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\")(false);\n// Module\nexports.push([module.i, \".wp-block-fusion-alert-sample-element {\\n  display: flex;\\n  justify-content: center;\\n  align-items: center;\\n  font-family: -apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto,\\n    Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;\\n}\\n\\n.wp-block-fusion-alert-sample-element > div {\\n  box-sizing: border-box;\\n  display: flex;\\n  justify-content: center;\\n  align-items: center;\\n}\\n\\n.wp-block-fusion-alert-sample-element-content {\\n  line-height: normal;\\n  font-size: 13px;\\n  color: #444;\\n}\\n\\n.wp-block-fusion-alert-sample-element-icon span {\\n  display: inline-block;\\n  margin-right: 0.5em;\\n  font-size: 1em;\\n}\\n\\n.fusion-builder-alert-controller {\\n  margin: 10px 0px;\\n  border-bottom: 1px solid #33333330;\\n  padding-bottom: 10px;\\n}\\n\\n.fusion-builder-alert-controller-heading h3 {\\n  font-weight: bold;\\n}\\n\\n.fusion-builder-alert-controller-heading .editor-rich-text {\\n  border: 1px solid #33333354;\\n  padding: 5px;\\n  box-sizing: border-box;\\n  border-radius: 3px;\\n}\\n\", \"\"]);\n\n\n\n//# sourceURL=webpack:///./src/assets/styles/main.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\n// css base code, injected by the css-loader\nmodule.exports = function (useSourceMap) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = cssWithMappingToString(item, useSourceMap);\n\n      if (item[2]) {\n        return '@media ' + item[2] + '{' + content + '}';\n      } else {\n        return content;\n      }\n    }).join('');\n  }; // import a list of modules into the list\n\n\n  list.i = function (modules, mediaQuery) {\n    if (typeof modules === 'string') {\n      modules = [[null, modules, '']];\n    }\n\n    var alreadyImportedModules = {};\n\n    for (var i = 0; i < this.length; i++) {\n      var id = this[i][0];\n\n      if (id != null) {\n        alreadyImportedModules[id] = true;\n      }\n    }\n\n    for (i = 0; i < modules.length; i++) {\n      var item = modules[i]; // skip already imported module\n      // this implementation is not 100% perfect for weird media query combinations\n      // when a module is imported multiple times with different media queries.\n      // I hope this will never occur (Hey this way we have smaller bundles)\n\n      if (item[0] == null || !alreadyImportedModules[item[0]]) {\n        if (mediaQuery && !item[2]) {\n          item[2] = mediaQuery;\n        } else if (mediaQuery) {\n          item[2] = '(' + item[2] + ') and (' + mediaQuery + ')';\n        }\n\n        list.push(item);\n      }\n    }\n  };\n\n  return list;\n};\n\nfunction cssWithMappingToString(item, useSourceMap) {\n  var content = item[1] || '';\n  var cssMapping = item[3];\n\n  if (!cssMapping) {\n    return content;\n  }\n\n  if (useSourceMap && typeof btoa === 'function') {\n    var sourceMapping = toComment(cssMapping);\n    var sourceURLs = cssMapping.sources.map(function (source) {\n      return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';\n    });\n    return [content].concat(sourceURLs).concat([sourceMapping]).join('\\n');\n  }\n\n  return [content].join('\\n');\n} // Adapted from convert-source-map (MIT)\n\n\nfunction toComment(sourceMap) {\n  // eslint-disable-next-line no-undef\n  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));\n  var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;\n  return '/*# ' + data + ' */';\n}\n\n//# sourceURL=webpack:///./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/*\n\tMIT License http://www.opensource.org/licenses/mit-license.php\n\tAuthor Tobias Koppers @sokra\n*/\n\nvar stylesInDom = {};\n\nvar\tmemoize = function (fn) {\n\tvar memo;\n\n\treturn function () {\n\t\tif (typeof memo === \"undefined\") memo = fn.apply(this, arguments);\n\t\treturn memo;\n\t};\n};\n\nvar isOldIE = memoize(function () {\n\t// Test for IE <= 9 as proposed by Browserhacks\n\t// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805\n\t// Tests for existence of standard globals is to allow style-loader\n\t// to operate correctly into non-standard environments\n\t// @see https://github.com/webpack-contrib/style-loader/issues/177\n\treturn window && document && document.all && !window.atob;\n});\n\nvar getTarget = function (target, parent) {\n  if (parent){\n    return parent.querySelector(target);\n  }\n  return document.querySelector(target);\n};\n\nvar getElement = (function (fn) {\n\tvar memo = {};\n\n\treturn function(target, parent) {\n                // If passing function in options, then use it for resolve \"head\" element.\n                // Useful for Shadow Root style i.e\n                // {\n                //   insertInto: function () { return document.querySelector(\"#foo\").shadowRoot }\n                // }\n                if (typeof target === 'function') {\n                        return target();\n                }\n                if (typeof memo[target] === \"undefined\") {\n\t\t\tvar styleTarget = getTarget.call(this, target, parent);\n\t\t\t// Special case to return head of iframe instead of iframe itself\n\t\t\tif (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n\t\t\t\ttry {\n\t\t\t\t\t// This will throw an exception if access to iframe is blocked\n\t\t\t\t\t// due to cross-origin restrictions\n\t\t\t\t\tstyleTarget = styleTarget.contentDocument.head;\n\t\t\t\t} catch(e) {\n\t\t\t\t\tstyleTarget = null;\n\t\t\t\t}\n\t\t\t}\n\t\t\tmemo[target] = styleTarget;\n\t\t}\n\t\treturn memo[target]\n\t};\n})();\n\nvar singleton = null;\nvar\tsingletonCounter = 0;\nvar\tstylesInsertedAtTop = [];\n\nvar\tfixUrls = __webpack_require__(/*! ./urls */ \"./node_modules/style-loader/lib/urls.js\");\n\nmodule.exports = function(list, options) {\n\tif (typeof DEBUG !== \"undefined\" && DEBUG) {\n\t\tif (typeof document !== \"object\") throw new Error(\"The style-loader cannot be used in a non-browser environment\");\n\t}\n\n\toptions = options || {};\n\n\toptions.attrs = typeof options.attrs === \"object\" ? options.attrs : {};\n\n\t// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>\n\t// tags it will allow on a page\n\tif (!options.singleton && typeof options.singleton !== \"boolean\") options.singleton = isOldIE();\n\n\t// By default, add <style> tags to the <head> element\n        if (!options.insertInto) options.insertInto = \"head\";\n\n\t// By default, add <style> tags to the bottom of the target\n\tif (!options.insertAt) options.insertAt = \"bottom\";\n\n\tvar styles = listToStyles(list, options);\n\n\taddStylesToDom(styles, options);\n\n\treturn function update (newList) {\n\t\tvar mayRemove = [];\n\n\t\tfor (var i = 0; i < styles.length; i++) {\n\t\t\tvar item = styles[i];\n\t\t\tvar domStyle = stylesInDom[item.id];\n\n\t\t\tdomStyle.refs--;\n\t\t\tmayRemove.push(domStyle);\n\t\t}\n\n\t\tif(newList) {\n\t\t\tvar newStyles = listToStyles(newList, options);\n\t\t\taddStylesToDom(newStyles, options);\n\t\t}\n\n\t\tfor (var i = 0; i < mayRemove.length; i++) {\n\t\t\tvar domStyle = mayRemove[i];\n\n\t\t\tif(domStyle.refs === 0) {\n\t\t\t\tfor (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();\n\n\t\t\t\tdelete stylesInDom[domStyle.id];\n\t\t\t}\n\t\t}\n\t};\n};\n\nfunction addStylesToDom (styles, options) {\n\tfor (var i = 0; i < styles.length; i++) {\n\t\tvar item = styles[i];\n\t\tvar domStyle = stylesInDom[item.id];\n\n\t\tif(domStyle) {\n\t\t\tdomStyle.refs++;\n\n\t\t\tfor(var j = 0; j < domStyle.parts.length; j++) {\n\t\t\t\tdomStyle.parts[j](item.parts[j]);\n\t\t\t}\n\n\t\t\tfor(; j < item.parts.length; j++) {\n\t\t\t\tdomStyle.parts.push(addStyle(item.parts[j], options));\n\t\t\t}\n\t\t} else {\n\t\t\tvar parts = [];\n\n\t\t\tfor(var j = 0; j < item.parts.length; j++) {\n\t\t\t\tparts.push(addStyle(item.parts[j], options));\n\t\t\t}\n\n\t\t\tstylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};\n\t\t}\n\t}\n}\n\nfunction listToStyles (list, options) {\n\tvar styles = [];\n\tvar newStyles = {};\n\n\tfor (var i = 0; i < list.length; i++) {\n\t\tvar item = list[i];\n\t\tvar id = options.base ? item[0] + options.base : item[0];\n\t\tvar css = item[1];\n\t\tvar media = item[2];\n\t\tvar sourceMap = item[3];\n\t\tvar part = {css: css, media: media, sourceMap: sourceMap};\n\n\t\tif(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});\n\t\telse newStyles[id].parts.push(part);\n\t}\n\n\treturn styles;\n}\n\nfunction insertStyleElement (options, style) {\n\tvar target = getElement(options.insertInto)\n\n\tif (!target) {\n\t\tthrow new Error(\"Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.\");\n\t}\n\n\tvar lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];\n\n\tif (options.insertAt === \"top\") {\n\t\tif (!lastStyleElementInsertedAtTop) {\n\t\t\ttarget.insertBefore(style, target.firstChild);\n\t\t} else if (lastStyleElementInsertedAtTop.nextSibling) {\n\t\t\ttarget.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);\n\t\t} else {\n\t\t\ttarget.appendChild(style);\n\t\t}\n\t\tstylesInsertedAtTop.push(style);\n\t} else if (options.insertAt === \"bottom\") {\n\t\ttarget.appendChild(style);\n\t} else if (typeof options.insertAt === \"object\" && options.insertAt.before) {\n\t\tvar nextSibling = getElement(options.insertAt.before, target);\n\t\ttarget.insertBefore(style, nextSibling);\n\t} else {\n\t\tthrow new Error(\"[Style Loader]\\n\\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\\n Must be 'top', 'bottom', or Object.\\n (https://github.com/webpack-contrib/style-loader#insertat)\\n\");\n\t}\n}\n\nfunction removeStyleElement (style) {\n\tif (style.parentNode === null) return false;\n\tstyle.parentNode.removeChild(style);\n\n\tvar idx = stylesInsertedAtTop.indexOf(style);\n\tif(idx >= 0) {\n\t\tstylesInsertedAtTop.splice(idx, 1);\n\t}\n}\n\nfunction createStyleElement (options) {\n\tvar style = document.createElement(\"style\");\n\n\tif(options.attrs.type === undefined) {\n\t\toptions.attrs.type = \"text/css\";\n\t}\n\n\tif(options.attrs.nonce === undefined) {\n\t\tvar nonce = getNonce();\n\t\tif (nonce) {\n\t\t\toptions.attrs.nonce = nonce;\n\t\t}\n\t}\n\n\taddAttrs(style, options.attrs);\n\tinsertStyleElement(options, style);\n\n\treturn style;\n}\n\nfunction createLinkElement (options) {\n\tvar link = document.createElement(\"link\");\n\n\tif(options.attrs.type === undefined) {\n\t\toptions.attrs.type = \"text/css\";\n\t}\n\toptions.attrs.rel = \"stylesheet\";\n\n\taddAttrs(link, options.attrs);\n\tinsertStyleElement(options, link);\n\n\treturn link;\n}\n\nfunction addAttrs (el, attrs) {\n\tObject.keys(attrs).forEach(function (key) {\n\t\tel.setAttribute(key, attrs[key]);\n\t});\n}\n\nfunction getNonce() {\n\tif (false) {}\n\n\treturn __webpack_require__.nc;\n}\n\nfunction addStyle (obj, options) {\n\tvar style, update, remove, result;\n\n\t// If a transform function was defined, run it on the css\n\tif (options.transform && obj.css) {\n\t    result = typeof options.transform === 'function'\n\t\t ? options.transform(obj.css) \n\t\t : options.transform.default(obj.css);\n\n\t    if (result) {\n\t    \t// If transform returns a value, use that instead of the original css.\n\t    \t// This allows running runtime transformations on the css.\n\t    \tobj.css = result;\n\t    } else {\n\t    \t// If the transform function returns a falsy value, don't add this css.\n\t    \t// This allows conditional loading of css\n\t    \treturn function() {\n\t    \t\t// noop\n\t    \t};\n\t    }\n\t}\n\n\tif (options.singleton) {\n\t\tvar styleIndex = singletonCounter++;\n\n\t\tstyle = singleton || (singleton = createStyleElement(options));\n\n\t\tupdate = applyToSingletonTag.bind(null, style, styleIndex, false);\n\t\tremove = applyToSingletonTag.bind(null, style, styleIndex, true);\n\n\t} else if (\n\t\tobj.sourceMap &&\n\t\ttypeof URL === \"function\" &&\n\t\ttypeof URL.createObjectURL === \"function\" &&\n\t\ttypeof URL.revokeObjectURL === \"function\" &&\n\t\ttypeof Blob === \"function\" &&\n\t\ttypeof btoa === \"function\"\n\t) {\n\t\tstyle = createLinkElement(options);\n\t\tupdate = updateLink.bind(null, style, options);\n\t\tremove = function () {\n\t\t\tremoveStyleElement(style);\n\n\t\t\tif(style.href) URL.revokeObjectURL(style.href);\n\t\t};\n\t} else {\n\t\tstyle = createStyleElement(options);\n\t\tupdate = applyToTag.bind(null, style);\n\t\tremove = function () {\n\t\t\tremoveStyleElement(style);\n\t\t};\n\t}\n\n\tupdate(obj);\n\n\treturn function updateStyle (newObj) {\n\t\tif (newObj) {\n\t\t\tif (\n\t\t\t\tnewObj.css === obj.css &&\n\t\t\t\tnewObj.media === obj.media &&\n\t\t\t\tnewObj.sourceMap === obj.sourceMap\n\t\t\t) {\n\t\t\t\treturn;\n\t\t\t}\n\n\t\t\tupdate(obj = newObj);\n\t\t} else {\n\t\t\tremove();\n\t\t}\n\t};\n}\n\nvar replaceText = (function () {\n\tvar textStore = [];\n\n\treturn function (index, replacement) {\n\t\ttextStore[index] = replacement;\n\n\t\treturn textStore.filter(Boolean).join('\\n');\n\t};\n})();\n\nfunction applyToSingletonTag (style, index, remove, obj) {\n\tvar css = remove ? \"\" : obj.css;\n\n\tif (style.styleSheet) {\n\t\tstyle.styleSheet.cssText = replaceText(index, css);\n\t} else {\n\t\tvar cssNode = document.createTextNode(css);\n\t\tvar childNodes = style.childNodes;\n\n\t\tif (childNodes[index]) style.removeChild(childNodes[index]);\n\n\t\tif (childNodes.length) {\n\t\t\tstyle.insertBefore(cssNode, childNodes[index]);\n\t\t} else {\n\t\t\tstyle.appendChild(cssNode);\n\t\t}\n\t}\n}\n\nfunction applyToTag (style, obj) {\n\tvar css = obj.css;\n\tvar media = obj.media;\n\n\tif(media) {\n\t\tstyle.setAttribute(\"media\", media)\n\t}\n\n\tif(style.styleSheet) {\n\t\tstyle.styleSheet.cssText = css;\n\t} else {\n\t\twhile(style.firstChild) {\n\t\t\tstyle.removeChild(style.firstChild);\n\t\t}\n\n\t\tstyle.appendChild(document.createTextNode(css));\n\t}\n}\n\nfunction updateLink (link, options, obj) {\n\tvar css = obj.css;\n\tvar sourceMap = obj.sourceMap;\n\n\t/*\n\t\tIf convertToAbsoluteUrls isn't defined, but sourcemaps are enabled\n\t\tand there is no publicPath defined then lets turn convertToAbsoluteUrls\n\t\ton by default.  Otherwise default to the convertToAbsoluteUrls option\n\t\tdirectly\n\t*/\n\tvar autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;\n\n\tif (options.convertToAbsoluteUrls || autoFixUrls) {\n\t\tcss = fixUrls(css);\n\t}\n\n\tif (sourceMap) {\n\t\t// http://stackoverflow.com/a/26603875\n\t\tcss += \"\\n/*# sourceMappingURL=data:application/json;base64,\" + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + \" */\";\n\t}\n\n\tvar blob = new Blob([css], { type: \"text/css\" });\n\n\tvar oldSrc = link.href;\n\n\tlink.href = URL.createObjectURL(blob);\n\n\tif(oldSrc) URL.revokeObjectURL(oldSrc);\n}\n\n\n//# sourceURL=webpack:///./node_modules/style-loader/lib/addStyles.js?");

/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("\n/**\n * When source maps are enabled, `style-loader` uses a link element with a data-uri to\n * embed the css on the page. This breaks all relative urls because now they are relative to a\n * bundle instead of the current page.\n *\n * One solution is to only use full urls, but that may be impossible.\n *\n * Instead, this function \"fixes\" the relative urls to be absolute according to the current page location.\n *\n * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.\n *\n */\n\nmodule.exports = function (css) {\n  // get current location\n  var location = typeof window !== \"undefined\" && window.location;\n\n  if (!location) {\n    throw new Error(\"fixUrls requires window.location\");\n  }\n\n\t// blank or null?\n\tif (!css || typeof css !== \"string\") {\n\t  return css;\n  }\n\n  var baseUrl = location.protocol + \"//\" + location.host;\n  var currentDir = baseUrl + location.pathname.replace(/\\/[^\\/]*$/, \"/\");\n\n\t// convert each url(...)\n\t/*\n\tThis regular expression is just a way to recursively match brackets within\n\ta string.\n\n\t /url\\s*\\(  = Match on the word \"url\" with any whitespace after it and then a parens\n\t   (  = Start a capturing group\n\t     (?:  = Start a non-capturing group\n\t         [^)(]  = Match anything that isn't a parentheses\n\t         |  = OR\n\t         \\(  = Match a start parentheses\n\t             (?:  = Start another non-capturing groups\n\t                 [^)(]+  = Match anything that isn't a parentheses\n\t                 |  = OR\n\t                 \\(  = Match a start parentheses\n\t                     [^)(]*  = Match anything that isn't a parentheses\n\t                 \\)  = Match a end parentheses\n\t             )  = End Group\n              *\\) = Match anything and then a close parens\n          )  = Close non-capturing group\n          *  = Match anything\n       )  = Close capturing group\n\t \\)  = Match a close parens\n\n\t /gi  = Get all matches, not the first.  Be case insensitive.\n\t */\n\tvar fixedCss = css.replace(/url\\s*\\(((?:[^)(]|\\((?:[^)(]+|\\([^)(]*\\))*\\))*)\\)/gi, function(fullMatch, origUrl) {\n\t\t// strip quotes (if they exist)\n\t\tvar unquotedOrigUrl = origUrl\n\t\t\t.trim()\n\t\t\t.replace(/^\"(.*)\"$/, function(o, $1){ return $1; })\n\t\t\t.replace(/^'(.*)'$/, function(o, $1){ return $1; });\n\n\t\t// already a full url? no change\n\t\tif (/^(#|data:|http:\\/\\/|https:\\/\\/|file:\\/\\/\\/|\\s*$)/i.test(unquotedOrigUrl)) {\n\t\t  return fullMatch;\n\t\t}\n\n\t\t// convert the url to a full url\n\t\tvar newUrl;\n\n\t\tif (unquotedOrigUrl.indexOf(\"//\") === 0) {\n\t\t  \t//TODO: should we add protocol?\n\t\t\tnewUrl = unquotedOrigUrl;\n\t\t} else if (unquotedOrigUrl.indexOf(\"/\") === 0) {\n\t\t\t// path should be relative to the base url\n\t\t\tnewUrl = baseUrl + unquotedOrigUrl; // already starts with '/'\n\t\t} else {\n\t\t\t// path should be relative to current directory\n\t\t\tnewUrl = currentDir + unquotedOrigUrl.replace(/^\\.\\//, \"\"); // Strip leading './'\n\t\t}\n\n\t\t// send back the fixed url(...)\n\t\treturn \"url(\" + JSON.stringify(newUrl) + \")\";\n\t});\n\n\t// send back the fixed css\n\treturn fixedCss;\n};\n\n\n//# sourceURL=webpack:///./node_modules/style-loader/lib/urls.js?");

/***/ }),

/***/ "./src/assets/styles/main.css":
/*!************************************!*\
  !*** ./src/assets/styles/main.css ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./main.css */ \"./node_modules/css-loader/dist/cjs.js!./src/assets/styles/main.css\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./src/assets/styles/main.css?");

/***/ }),

/***/ "./src/components/Container/index.js":
/*!*******************************************!*\
  !*** ./src/components/Container/index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar ShortcodeContainer = function ShortcodeContainer(props) {\n  var className = props.className,\n      children = props.children;\n  var template = children.template;\n  return React.createElement(\"div\", {\n    className: className,\n    dangerouslySetInnerHTML: {\n      __html: template\n    }\n  });\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (ShortcodeContainer);\n\n//# sourceURL=webpack:///./src/components/Container/index.js?");

/***/ }),

/***/ "./src/components/Settings/index.js":
/*!******************************************!*\
  !*** ./src/components/Settings/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ \"./node_modules/@babel/runtime/helpers/extends.js\");\n/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _controllers_Select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../controllers/Select */ \"./src/components/controllers/Select/index.js\");\n/* harmony import */ var _controllers_Radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../controllers/Radio */ \"./src/components/controllers/Radio/index.js\");\n/* harmony import */ var _controllers_Text__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../controllers/Text */ \"./src/components/controllers/Text/index.js\");\n/* harmony import */ var _controllers_TextArea__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../controllers/TextArea */ \"./src/components/controllers/TextArea/index.js\");\n/* harmony import */ var _controllers_Checkbox__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../controllers/Checkbox */ \"./src/components/controllers/Checkbox/index.js\");\n/* harmony import */ var _controllers_Color__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../controllers/Color */ \"./src/components/controllers/Color/index.js\");\n\n\n\n\n\n\n\n\nvar renderControllers = function renderControllers(settingsList, onUpdate) {\n  var controllers = [];\n\n  for (var controller in settingsList) {\n    var currentController = settingsList[controller];\n\n    switch (currentController.type) {\n      case \"select\":\n        controllers.push(React.createElement(_controllers_Select__WEBPACK_IMPORTED_MODULE_1__[\"default\"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, currentController, {\n          onUpdate: onUpdate\n        })));\n        break;\n\n      case \"radio_button_set\":\n        controllers.push(React.createElement(_controllers_Radio__WEBPACK_IMPORTED_MODULE_2__[\"default\"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, currentController, {\n          onUpdate: onUpdate\n        })));\n        break;\n\n      case \"checkbox_button_set\":\n        controllers.push(React.createElement(_controllers_Checkbox__WEBPACK_IMPORTED_MODULE_5__[\"default\"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, currentController, {\n          onUpdate: onUpdate\n        })));\n        break;\n\n      case \"textfield\":\n        controllers.push(React.createElement(_controllers_Text__WEBPACK_IMPORTED_MODULE_3__[\"default\"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, currentController, {\n          onUpdate: onUpdate\n        })));\n        break;\n\n      case \"tinymce\":\n        controllers.push(React.createElement(_controllers_TextArea__WEBPACK_IMPORTED_MODULE_4__[\"default\"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, currentController, {\n          onUpdate: onUpdate\n        })));\n        break;\n\n      case \"colorpickeralpha\":\n        controllers.push(React.createElement(_controllers_Color__WEBPACK_IMPORTED_MODULE_6__[\"default\"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, currentController, {\n          onUpdate: onUpdate\n        })));\n        break;\n\n      default:\n        controllers.push(null);\n        break;\n    }\n  }\n\n  return controllers;\n};\n\nvar SettingsContainer = function SettingsContainer(props) {\n  var _wp$editor = wp.editor,\n      RichText = _wp$editor.RichText,\n      BlockControls = _wp$editor.BlockControls,\n      AlignmentToolbar = _wp$editor.AlignmentToolbar,\n      InspectorControls = _wp$editor.InspectorControls;\n  var settingsList = props.settingsList,\n      onUpdate = props.onUpdate;\n  return React.createElement(InspectorControls, null, renderControllers(settingsList, onUpdate));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (SettingsContainer);\n\n//# sourceURL=webpack:///./src/components/Settings/index.js?");

/***/ }),

/***/ "./src/components/controllers/Checkbox/index.js":
/*!******************************************************!*\
  !*** ./src/components/controllers/Checkbox/index.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"./node_modules/@babel/runtime/helpers/defineProperty.js\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar check = function check(item, defaultValue) {\n  return defaultValue.splice(defaultValue.indexOf(item), 1);\n};\n\nvar renderButtons = function renderButtons(value, onUpdate, belongsTo, defaultValue) {\n  var buttons = [];\n  var Button = wp.components.Button;\n\n  var _loop = function _loop(item) {\n    buttons.push(React.createElement(Button, {\n      isPrimary: true,\n      onClick: function onClick() {\n        onUpdate(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, belongsTo, check(item, defaultValue)));\n      }\n    }, value[item]));\n  };\n\n  for (var item in value) {\n    _loop(item);\n  }\n\n  return buttons;\n};\n\nvar Checkbox = function Checkbox(props) {\n  var belongsTo = props.belongsTo,\n      defaultValue = props.default,\n      description = props.description,\n      heading = props.heading,\n      value = props.value,\n      onUpdate = props.onUpdate;\n  var ButtonGroup = wp.components.ButtonGroup;\n  return React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller\"\n  }, React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(\"h3\", null, heading), React.createElement(\"p\", {\n    dangerouslySetInnerHTML: {\n      __html: description\n    }\n  })), React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(ButtonGroup, null, renderButtons(value, onUpdate, belongsTo, defaultValue))));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Checkbox);\n\n//# sourceURL=webpack:///./src/components/controllers/Checkbox/index.js?");

/***/ }),

/***/ "./src/components/controllers/Color/index.js":
/*!***************************************************!*\
  !*** ./src/components/controllers/Color/index.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"./node_modules/@babel/runtime/helpers/defineProperty.js\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar Color = function Color(props) {\n  var belongsTo = props.belongsTo,\n      defaultValue = props.default,\n      description = props.description,\n      heading = props.heading,\n      value = props.value,\n      onUpdate = props.onUpdate;\n  var ColorPicker = wp.components.ColorPicker;\n  return React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller\"\n  }, React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(\"h3\", null, heading), React.createElement(\"p\", {\n    dangerouslySetInnerHTML: {\n      __html: description\n    }\n  })), React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(ColorPicker, {\n    onChangeComplete: function onChangeComplete(_ref) {\n      var hex = _ref.hex;\n      return onUpdate(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, belongsTo, hex));\n    },\n    disableAlpha: true\n  })));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Color);\n\n//# sourceURL=webpack:///./src/components/controllers/Color/index.js?");

/***/ }),

/***/ "./src/components/controllers/Radio/index.js":
/*!***************************************************!*\
  !*** ./src/components/controllers/Radio/index.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"./node_modules/@babel/runtime/helpers/defineProperty.js\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar renderButtons = function renderButtons(value, onUpdate, belongsTo) {\n  var buttons = [];\n  var Button = wp.components.Button;\n\n  var _loop = function _loop(item) {\n    buttons.push(React.createElement(Button, {\n      isPrimary: true,\n      onClick: function onClick() {\n        onUpdate(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, belongsTo, item));\n      }\n    }, value[item]));\n  };\n\n  for (var item in value) {\n    _loop(item);\n  }\n\n  return buttons;\n};\n\nvar Radio = function Radio(props) {\n  var belongsTo = props.belongsTo,\n      defaultValue = props.default,\n      description = props.description,\n      heading = props.heading,\n      value = props.value,\n      onUpdate = props.onUpdate;\n  var ButtonGroup = wp.components.ButtonGroup;\n  return React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller\"\n  }, React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(\"h3\", null, heading), React.createElement(\"p\", {\n    dangerouslySetInnerHTML: {\n      __html: description\n    }\n  })), React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(ButtonGroup, null, renderButtons(value, onUpdate, belongsTo))));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Radio);\n\n//# sourceURL=webpack:///./src/components/controllers/Radio/index.js?");

/***/ }),

/***/ "./src/components/controllers/Select/index.js":
/*!****************************************************!*\
  !*** ./src/components/controllers/Select/index.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"./node_modules/@babel/runtime/helpers/defineProperty.js\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar renderItems = function renderItems(value, onUpdate, belongsTo) {\n  var items = [];\n\n  var _loop = function _loop(item) {\n    items.push({\n      title: value[item],\n      onClick: function onClick() {\n        return onUpdate(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, belongsTo, item));\n      }\n    });\n  };\n\n  for (var item in value) {\n    _loop(item);\n  }\n\n  return items;\n};\n\nvar Select = function Select(props) {\n  var belongsTo = props.belongsTo,\n      defaultValue = props.default,\n      description = props.description,\n      heading = props.heading,\n      value = props.value,\n      onUpdate = props.onUpdate;\n  var DropdownMenu = wp.components.DropdownMenu;\n  return React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller\"\n  }, React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(\"h3\", null, heading), React.createElement(\"p\", {\n    dangerouslySetInnerHTML: {\n      __html: description\n    }\n  })), React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(DropdownMenu, {\n    label: heading,\n    controls: renderItems(value, onUpdate, belongsTo)\n  })));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Select);\n\n//# sourceURL=webpack:///./src/components/controllers/Select/index.js?");

/***/ }),

/***/ "./src/components/controllers/Text/index.js":
/*!**************************************************!*\
  !*** ./src/components/controllers/Text/index.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"./node_modules/@babel/runtime/helpers/defineProperty.js\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar Text = function Text(props) {\n  var belongsTo = props.belongsTo,\n      defaultValue = props.default,\n      description = props.description,\n      heading = props.heading,\n      value = props.value,\n      onUpdate = props.onUpdate;\n  var TextControl = wp.components.TextControl;\n  return React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller\"\n  }, React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(\"h3\", null, heading), React.createElement(\"p\", {\n    dangerouslySetInnerHTML: {\n      __html: description\n    }\n  })), React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(TextControl, {\n    label: heading,\n    onChange: function onChange(id) {\n      return onUpdate(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, belongsTo, id));\n    }\n  })));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Text);\n\n//# sourceURL=webpack:///./src/components/controllers/Text/index.js?");

/***/ }),

/***/ "./src/components/controllers/TextArea/index.js":
/*!******************************************************!*\
  !*** ./src/components/controllers/TextArea/index.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"./node_modules/@babel/runtime/helpers/defineProperty.js\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar TextArea = function TextArea(props) {\n  var belongsTo = props.belongsTo,\n      defaultValue = props.default,\n      description = props.description,\n      heading = props.heading,\n      value = props.value,\n      onUpdate = props.onUpdate;\n  setTimeout(function () {\n    tinyMCE.init({\n      selector: \".fusion-builder-alert-controller-editor\",\n      invalid_elements: \"p\",\n      init_instance_callback: function init_instance_callback(editor) {\n        editor.on(\"Blur\", function (e) {\n          var content = editor.getContent().replace(/<p>/g, \"\").replace(/<\\/p>/g, \"\");\n          onUpdate(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, belongsTo, content));\n        });\n      }\n    });\n  }, 100);\n  return React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller\"\n  }, React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(\"h3\", null, heading), React.createElement(\"p\", {\n    dangerouslySetInnerHTML: {\n      __html: description\n    }\n  })), React.createElement(\"div\", {\n    className: \"fusion-builder-alert-controller-heading\"\n  }, React.createElement(\"textarea\", {\n    className: \"fusion-builder-alert-controller-editor\"\n  })));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (TextArea);\n\n//# sourceURL=webpack:///./src/components/controllers/TextArea/index.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/objectSpread */ \"./node_modules/@babel/runtime/helpers/objectSpread.js\");\n/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _components_Container__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/Container */ \"./src/components/Container/index.js\");\n/* harmony import */ var _components_Settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/Settings */ \"./src/components/Settings/index.js\");\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utils */ \"./src/utils/index.js\");\n/* harmony import */ var _assets_styles_main_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./assets/styles/main.css */ \"./src/assets/styles/main.css\");\n/* harmony import */ var _assets_styles_main_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_styles_main_css__WEBPACK_IMPORTED_MODULE_4__);\n\n\n\n\n\nvar registerBlockType = wp.blocks.registerBlockType;\n\nvar _convertFusionElement = Object(_utils__WEBPACK_IMPORTED_MODULE_3__[\"convertFusionElementsToGutenberg\"])(fusionAllElements.fusion_alert),\n    title = _convertFusionElement.title,\n    attributes = _convertFusionElement.attributes,\n    settings = _convertFusionElement.settings;\n\nregisterBlockType(\"fusion/alert-sample-element-12svssd\", {\n  title: title,\n  icon: \"warning\",\n  category: \"layout\",\n  attributes: attributes,\n  edit: function edit(_ref) {\n    var attributes = _ref.attributes,\n        className = _ref.className,\n        setAttributes = _ref.setAttributes;\n    var Fragment = wp.element.Fragment;\n    var content = attributes.content;\n    var template = Object(_utils__WEBPACK_IMPORTED_MODULE_3__[\"getTemplate\"])(_babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0___default()({}, attributes, {\n      element_content: content\n    }));\n    return React.createElement(Fragment, null, React.createElement(_components_Settings__WEBPACK_IMPORTED_MODULE_2__[\"default\"], {\n      settingsList: settings,\n      onUpdate: setAttributes\n    }), React.createElement(_components_Container__WEBPACK_IMPORTED_MODULE_1__[\"default\"], {\n      className: className\n    }, {\n      template: template\n    }));\n  },\n  save: function save(_ref2) {\n    var attributes = _ref2.attributes,\n        className = _ref2.className;\n    var content = attributes.content;\n    var wordpressAttributes = \"\";\n\n    for (var attribute in attributes) {\n      if (attribute !== \"content\") {\n        wordpressAttributes += \"\".concat(attribute, \"=\\\"\").concat(attributes[attribute], \"\\\" \");\n      }\n    }\n\n    return \"[fusion_alert \".concat(wordpressAttributes, \" ]\").concat(content, \"[/fusion_alert]\");\n  }\n});\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/utils/index.js":
/*!****************************!*\
  !*** ./src/utils/index.js ***!
  \****************************/
/*! exports provided: getTemplate, convertFusionElementsToGutenberg */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getTemplate\", function() { return getTemplate; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"convertFusionElementsToGutenberg\", function() { return convertFusionElementsToGutenberg; });\n/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/typeof */ \"./node_modules/@babel/runtime/helpers/typeof.js\");\n/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar isMobile = function isMobile() {\n  if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {\n    return true;\n  } else {\n    return false;\n  }\n};\n\nvar getDefaultValue = function getDefaultValue(defaultValue, value) {\n  var finalValue;\n\n  if (defaultValue) {\n    if (Array.isArray(defaultValue)) {\n      finalValue = defaultValue.join(\",\");\n    } else {\n      finalValue = defaultValue;\n    }\n  } else if (Array.isArray(value)) {\n    finalValue = value[0];\n  } else if (_babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default()(value) === \"object\") {\n    finalValue = \"\";\n  } else if (typeof value === \"string\") {\n    finalValue = value;\n  } else {\n    finalValue = null;\n  }\n\n  return finalValue;\n};\n\nvar getParams = function getParams(_ref) {\n  var type = _ref.type,\n      defaultValue = _ref.default,\n      value = _ref.value;\n  return {\n    type: type === \"tinymce\" ? \"html\" : \"string\",\n    default: getDefaultValue(defaultValue, value)\n  };\n};\n\nvar getSettings = function getSettings(_ref2) {\n  var type = _ref2.type,\n      param_name = _ref2.param_name,\n      description = _ref2.description,\n      heading = _ref2.heading,\n      defaultValue = _ref2.default,\n      value = _ref2.value;\n  return {\n    type: type,\n    belongsTo: param_name === \"element_content\" ? \"content\" : param_name,\n    description: description,\n    heading: heading,\n    default: defaultValue,\n    value: value ? value : defaultValue\n  };\n};\n\nvar extractInfoFromShortcode = function extractInfoFromShortcode(params) {\n  var convertedParams = {};\n  var convertedSetting = {};\n\n  for (var param in params) {\n    var currentParam = params[param];\n    convertedParams[currentParam.param_name === \"element_content\" ? \"content\" : currentParam.param_name] = getParams(currentParam);\n    convertedSetting[currentParam.param_name] = getSettings(currentParam);\n  }\n\n  return {\n    attributes: convertedParams,\n    settings: convertedSetting\n  };\n};\n\nvar getTemplate = function getTemplate(params) {\n  var templateSection = jQuery(\"#fusion-builder-block-module-alert-preview-template\").html();\n\n  var compiledTemplate = _.template(templateSection, {\n    evaluate: /<#([\\s\\S]+?)#>/g,\n    interpolate: /\\{\\{\\{([\\s\\S]+?)\\}\\}\\}/g,\n    escape: /\\{\\{([^\\}]+?)\\}\\}(?!\\})/g\n  });\n\n  return compiledTemplate({\n    params: params\n  });\n};\nvar convertFusionElementsToGutenberg = function convertFusionElementsToGutenberg(shortcodeInfo) {\n  var title = shortcodeInfo.name,\n      params = shortcodeInfo.params;\n\n  var _extractInfoFromShort = extractInfoFromShortcode(params),\n      attributes = _extractInfoFromShort.attributes,\n      settings = _extractInfoFromShort.settings;\n\n  return {\n    attributes: attributes,\n    title: title,\n    settings: settings\n  };\n};\n\n//# sourceURL=webpack:///./src/utils/index.js?");

/***/ })

/******/ });