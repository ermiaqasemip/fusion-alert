const { createElement, Fragment } = wp.element;
const { ColorPalette, DropdownMenu, CheckboxControl } = wp.components;

const WordpressComponents = {
  ColorPalette,
  DropdownMenu,
  CheckboxControl,
  RichText,
  BlockControls,
  AlignmentToolbar,
  InspectorControls,
  registerBlockType,
  createElement,
  Fragment
};

export default WordpressComponents;
