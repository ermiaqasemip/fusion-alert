import Select from "./../controllers/Select";
import Radio from "./../controllers/Radio";
import Text from "./../controllers/Text";
import TextArea from "./../controllers/TextArea";
import Checkbox from "./../controllers/Checkbox";
import Color from "./../controllers/Color";

const renderControllers = (settingsList, onUpdate) => {
  let controllers = [];
  for (let controller in settingsList) {
    const currentController = settingsList[controller];
    switch (currentController.type) {
      case "select":
        controllers.push(<Select {...currentController} onUpdate={onUpdate} />);
        break;
      case "radio_button_set":
        controllers.push(<Radio {...currentController} onUpdate={onUpdate} />);
        break;
      case "checkbox_button_set":
        controllers.push(
          <Checkbox {...currentController} onUpdate={onUpdate} />
        );
        break;
      case "textfield":
        controllers.push(<Text {...currentController} onUpdate={onUpdate} />);
        break;
      case "tinymce":
        controllers.push(
          <TextArea {...currentController} onUpdate={onUpdate} />
        );
        break;
      case "colorpickeralpha":
        controllers.push(<Color {...currentController} onUpdate={onUpdate} />);
        break;
      default:
        controllers.push(null);
        break;
    }
  }
  return controllers;
};

const SettingsContainer = props => {
  const {
    RichText,
    BlockControls,
    AlignmentToolbar,
    InspectorControls
  } = wp.editor;
  const { settingsList, onUpdate } = props;
  return (
    <InspectorControls>
      {renderControllers(settingsList, onUpdate)}
    </InspectorControls>
  );
};

export default SettingsContainer;
