const ShortcodeContainer = props => {
  const { className, children } = props;
  const { template } = children;
  return (
    <div className={className} dangerouslySetInnerHTML={{ __html: template }} />
  );
};

export default ShortcodeContainer;
