const renderButtons = (value, onUpdate, belongsTo) => {
  let buttons = [];
  const { Button } = wp.components;
  for (let item in value) {
    buttons.push(
      <Button
        isPrimary
        onClick={() => {
          onUpdate({ [belongsTo]: item });
        }}
      >
        {value[item]}
      </Button>
    );
  }
  return buttons;
};

const Radio = props => {
  const {
    belongsTo,
    default: defaultValue,
    description,
    heading,
    value,
    onUpdate
  } = props;
  const { ButtonGroup } = wp.components;
  return (
    <div className="fusion-builder-alert-controller">
      <div className="fusion-builder-alert-controller-heading">
        <h3>{heading}</h3>
        <p dangerouslySetInnerHTML={{ __html: description }} />
      </div>
      <div className="fusion-builder-alert-controller-heading">
        <ButtonGroup>{renderButtons(value, onUpdate, belongsTo)}</ButtonGroup>
      </div>
    </div>
  );
};

export default Radio;
