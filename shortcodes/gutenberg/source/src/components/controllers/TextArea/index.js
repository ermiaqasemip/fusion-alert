const TextArea = props => {
  const {
    belongsTo,
    default: defaultValue,
    description,
    heading,
    value,
    onUpdate
  } = props;

  setTimeout(() => {
    tinyMCE.init({
      selector: ".fusion-builder-alert-controller-editor",
      invalid_elements: "p",
      init_instance_callback: function(editor) {
        editor.on("Blur", function(e) {
          const content = editor
            .getContent()
            .replace(/<p>/g, "")
            .replace(/<\/p>/g, "");

          onUpdate({ [belongsTo]: content });
        });
      }
    });
  }, 100);

  return (
    <div className="fusion-builder-alert-controller">
      <div className="fusion-builder-alert-controller-heading">
        <h3>{heading}</h3>
        <p dangerouslySetInnerHTML={{ __html: description }} />
      </div>
      <div className="fusion-builder-alert-controller-heading">
        <textarea className="fusion-builder-alert-controller-editor" />
      </div>
    </div>
  );
};

export default TextArea;
