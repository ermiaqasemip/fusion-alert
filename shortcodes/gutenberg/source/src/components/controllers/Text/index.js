const Text = props => {
  const {
    belongsTo,
    default: defaultValue,
    description,
    heading,
    value,
    onUpdate
  } = props;
  const { TextControl } = wp.components;
  return (
    <div className="fusion-builder-alert-controller">
      <div className="fusion-builder-alert-controller-heading">
        <h3>{heading}</h3>
        <p dangerouslySetInnerHTML={{ __html: description }} />
      </div>
      <div className="fusion-builder-alert-controller-heading">
        <TextControl
          label={heading}
          onChange={id => onUpdate({ [belongsTo]: id })}
        />
      </div>
    </div>
  );
};

export default Text;
