const Color = props => {
  const {
    belongsTo,
    default: defaultValue,
    description,
    heading,
    value,
    onUpdate
  } = props;
  const { ColorPicker } = wp.components;
  return (
    <div className="fusion-builder-alert-controller">
      <div className="fusion-builder-alert-controller-heading">
        <h3>{heading}</h3>
        <p dangerouslySetInnerHTML={{ __html: description }} />
      </div>
      <div className="fusion-builder-alert-controller-heading">
        <ColorPicker
          onChangeComplete={({ hex }) => onUpdate({ [belongsTo]: hex })}
          disableAlpha
        />
      </div>
    </div>
  );
};

export default Color;
