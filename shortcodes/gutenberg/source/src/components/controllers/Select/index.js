const renderItems = (value, onUpdate, belongsTo) => {
  const items = [];
  for (let item in value) {
    items.push({
      title: value[item],
      onClick: () => onUpdate({ [belongsTo]: item })
    });
  }
  return items;
};

const Select = props => {
  const {
    belongsTo,
    default: defaultValue,
    description,
    heading,
    value,
    onUpdate
  } = props;
  const { DropdownMenu } = wp.components;
  return (
    <div className="fusion-builder-alert-controller">
      <div className="fusion-builder-alert-controller-heading">
        <h3>{heading}</h3>
        <p dangerouslySetInnerHTML={{ __html: description }} />
      </div>
      <div className="fusion-builder-alert-controller-heading">
        <DropdownMenu
          label={heading}
          controls={renderItems(value, onUpdate, belongsTo)}
        />
      </div>
    </div>
  );
};

export default Select;
