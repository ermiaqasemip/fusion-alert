import ShortcodeContainer from "./components/Container";
import SettingsContainer from "./components/Settings";
import { convertFusionElementsToGutenberg, getTemplate } from "./utils";
import "./assets/styles/main.css";

const { registerBlockType } = wp.blocks;
const { title, attributes, settings } = convertFusionElementsToGutenberg(
  fusionAllElements.fusion_alert
);

registerBlockType("fusion/alert-sample-element-12svssd", {
  title,
  icon: "warning",
  category: "layout",
  attributes,
  edit({ attributes, className, setAttributes }) {
    const { Fragment } = wp.element;
    const { content } = attributes;
    const template = getTemplate({
      ...attributes,
      element_content: content
    });
    return (
      <Fragment>
        <SettingsContainer settingsList={settings} onUpdate={setAttributes} />
        <ShortcodeContainer className={className}>
          {{ template }}
        </ShortcodeContainer>
      </Fragment>
    );
  },
  save({ attributes, className }) {
    const { content } = attributes;
    let wordpressAttributes = "";
    for (let attribute in attributes) {
      if (attribute !== "content") {
        wordpressAttributes += `${attribute}="${attributes[attribute]}" `;
      }
    }
    return `[fusion_alert ${wordpressAttributes} ]${content}[/fusion_alert]`;
  }
});
