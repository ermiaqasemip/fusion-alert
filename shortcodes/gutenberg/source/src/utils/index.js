const isMobile = () => {
  if (
    navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i)
  ) {
    return true;
  } else {
    return false;
  }
};

const getDefaultValue = (defaultValue, value) => {
  let finalValue;
  if (defaultValue) {
    if (Array.isArray(defaultValue)) {
      finalValue = defaultValue.join(",");
    } else {
      finalValue = defaultValue;
    }
  } else if (Array.isArray(value)) {
    finalValue = value[0];
  } else if (typeof value === "object") {
    finalValue = "";
  } else if (typeof value === "string") {
    finalValue = value;
  } else {
    finalValue = null;
  }
  return finalValue;
};

const getParams = ({ type, default: defaultValue, value }) => {
  return {
    type: type === "tinymce" ? "html" : "string",
    default: getDefaultValue(defaultValue, value)
  };
};

const getSettings = ({
  type,
  param_name,
  description,
  heading,
  default: defaultValue,
  value
}) => {
  return {
    type,
    belongsTo: param_name === "element_content" ? "content" : param_name,
    description,
    heading,
    default: defaultValue,
    value: value ? value : defaultValue
  };
};

const extractInfoFromShortcode = params => {
  let convertedParams = {};
  let convertedSetting = {};
  for (let param in params) {
    const currentParam = params[param];
    convertedParams[
      currentParam.param_name === "element_content"
        ? "content"
        : currentParam.param_name
    ] = getParams(currentParam);
    convertedSetting[currentParam.param_name] = getSettings(currentParam);
  }
  return {
    attributes: convertedParams,
    settings: convertedSetting
  };
};

export const getTemplate = params => {
  const templateSection = jQuery(
    "#fusion-builder-block-module-alert-preview-template"
  ).html();
  const compiledTemplate = _.template(templateSection, {
    evaluate: /<#([\s\S]+?)#>/g,
    interpolate: /\{\{\{([\s\S]+?)\}\}\}/g,
    escape: /\{\{([^\}]+?)\}\}(?!\})/g
  });
  return compiledTemplate({ params });
};

export const convertFusionElementsToGutenberg = shortcodeInfo => {
  const { name: title, params } = shortcodeInfo;
  const { attributes, settings } = extractInfoFromShortcode(params);
  return { attributes, title, settings };
};
